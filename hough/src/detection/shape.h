#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <utility>

using extrem_point = std::pair<cv::Point, cv::Point>;

extern cv::Mat element_dilate;
extern cv::Mat element_erode;

/**
 * draw an arrow at the position @p to the given direction @p
 * @img the texture who is displayed the arrow
 * @p the center position
 * @q the direction of the arrow
 * @colour the arrow colour
 * @scale the arrow scale
 **/
void drawAxis(
    cv::Mat& img, cv::Point p, cv::Point q,
    cv::Scalar colour, const float scale = 0.2
);

/**
 * @src the texture who is write the axis orientation using @drawAxis
 * @return the angle orientation in degree from @pts calculated by mean square approximation
 **/
float getOrientation(
    const std::vector<cv::Point> &pts,
    cv::Mat &img
);

/**
 * calculate a structuring element matrix
 * with cv::Size(2*erosion_size+1, 2*erosion_size+1)
 * and cv::Point(erosion_size, erosion_size)
 * @return cv::getStructuringElement
 * @erosion_size the size on offset used by cv::getStructuringElement
 **/
cv::Mat getErodeDilateMatrix(
    unsigned int erosion_size
);

/**
 * this function calculate the ratio between the surface area and the convex hull area
 * @return surface_area / convex_hull_area
 **/
float getSurfaceSolidity(
    const cv::Mat &src,
    const std::vector<cv::Point> &contour
);

/**
 * @return the fourst extrem point of the contours
 * out.first = width_min_max
 * out.second = height_min_max
 **/
std::pair<extrem_point, extrem_point> getExtremPoint(
    const std::vector<cv::Point>&
);

/**
 * @src threshold it with cv::ADAPTIVE_THRESH_GAUSSIAN_C and the threshold value 200 (empiricaly tested) to @threshold_output
 * @threshold_output apply a cv::dilate and cv::erode at the end to reconnect the head from the corps
 **/
void thresholdAndMorph(
    const cv::Mat &src,
    cv::Mat &threshold_output
);

/**
 * select the best contours of the given threshold_output
 * @contours is all contours founds by cv::findContours
 * @minEllipse is all cv::fitEllipse when the contours[i] is valide
 * @return the index of the best contours, or -1 if no one
 **/
int findAreaAndFitEllipse(
    cv::Mat &threshold_output,
    std::vector<std::vector<cv::Point>> &contours,
    std::vector<cv::RotatedRect> &minEllipse
);

/**
 * https://stackoverflow.com/a/30418912/5008845
 **/
cv::Rect findMinRect(const cv::Mat1b& src);

/**
 * try to find the largest rectangle inside a non-convex polygon
 * this is a difficult task with a complexity of O(n^4)
 * ! do not over use this methode
 * @return the best candidate inside the binary image @src
 **/
cv::RotatedRect largestRectInNonConvexPoly(const cv::Mat1b& src);
