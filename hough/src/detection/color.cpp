#include "color.h"
#include "shape.h"
#include "../tools/config.h"

#include "opencv2/xphoto.hpp"
#include <iostream>

void create_detection_planes(const cv::Mat &original, std::vector<cv::Mat> &planes)
{
		cv::Mat src, green;
    
    //! add 2 independent plane r,b
    //! form the saturated texture
    //! this is the initial idea
		{
				src = original.clone();
        //cv::xphoto::balanceWhite(original, src, cv::xphoto::WHITE_BALANCE_SIMPLE);
				image_processing(src, false);
				cv::split(src, planes);
				
				cvtColor(planes[0], planes[0], CV_GRAY2BGR);
				cvtColor(planes[1], green, CV_GRAY2BGR);
				cvtColor(planes[2], planes[2], CV_GRAY2BGR);
				
        // remove the useless green plane
				planes.erase(planes.begin()+1);
		}
		
		//! add red-green difference
    //! can help to detect sleeping activity
    //! increase sleeping detection by 5%
		{
				cv::absdiff(planes[0], planes[1], src);
				src = cv::Scalar::all(255) - src;
				planes.push_back(src);
    }
		
		//! add desaturated image
    //! help to detect goat when the previous method failed
    //! increase goat detection by 3.87%
		{
				src = original.clone();
        //cv::xphoto::balanceWhite(original, src, cv::xphoto::WHITE_BALANCE_SIMPLE);
				error_processing(src);
				planes.push_back(src);
		}
}

void image_processing(cv::Mat &src, bool only_detection)
{
    auto s1 = cv::Size(Config::blur_size[0]+1, Config::blur_size[0]+1);
    auto s2 = cv::Size(Config::blur_size[1]+1, Config::blur_size[1]+1);
    
    cv::Mat src_gray;

    //! the 2 next instruction increase activity recognition by 3%
    //! used to enhance the edge of the goat
    if(Config::sharpen)
        sharpen(src);
    
    //! used to reduce the noise introduced by sharpen in the weed
    blur(src, src, s1);
    
    //! reduce the weed and burned weed also increase goat contrast after saturation
    //! also increase detection and activity by 1%
    if(Config::remove_yellow)
        remove_yellow(src);
    
    //! transform the color space to retrieve the Hue
    saturate(src);
    
    if(only_detection)
    {
        //! this first bluring allow to remove weed noise
        blur(src, src, s1);
        
        //! remove weed for edge detection (thresholding)
        //! value found in goat histogram
        //! but we have reduced the green threshold for dectection
        
        remove_green_major(
            src,
            Config::remove_green_threshold[0],
            Config::remove_green_max[0]
        );
        
        // only need a huge blured blob for ellipse fetting
        // do not care about the activity recognition
        blur(src, src, s2);
    }
    else
    {
        //! propagate region to try to reconnect the goat neck
        //! bad idea, this reduce the activity recognition by 2%
        //blur(src, src, cv::Size(3,3));
        
        //! unification of similar colors (region)
        //! also increase detection and activity by 1%
        if(Config::bilateral)
        {
            bilateralFilter(src, src_gray, 20, 75, 75);
            src = src_gray.clone();
        }
        
        //! propagate color inside region
        //! usefull for leter when geting intersection (violet)
        //! when to contour detection is sparse
        //! also increase activity regognition by 4%
        blur(src, src, s1);
    
        //! remove weed for edge detection (thresholding)
        //! value found in goat histogram
        
        remove_green_major(
            src,
            Config::remove_green_threshold[1],
            Config::remove_green_max[1]
        );
    }
}

void error_processing(cv::Mat &src)
{
    auto s1 = cv::Size(Config::blur_size[0]+1, Config::blur_size[0]+1);
    
    cv::Mat src_gray;
    
    //! propagate the hair crest
    cv::blur(src, src, s1);
    
    //cv::namedWindow("0", cv::WINDOW_NORMAL);
    //cv::imshow("0", src);
    
    //! get only the saturation
    desaturate(src);
    
    //cv::namedWindow("1", cv::WINDOW_NORMAL);
    //cv::imshow("1", src);
    
    //! enhancement the colorspace
    cv::normalize(src, src, 0, 255, cv::NORM_MINMAX);
    
    //cv::namedWindow("2", cv::WINDOW_NORMAL);
    //cv::imshow("2", src);
    
    //! increase background difference
    cv::addWeighted(src, 1, src, 3, 0.0, src);
    
    //cv::namedWindow("3", cv::WINDOW_NORMAL);
    //cv::imshow("3", src);
    
    if(Config::bilateral)
    {
        //! unification of similar colors (crest region)
        cv::bilateralFilter(src, src_gray, 20, 75, 75);
        src = src_gray.clone();
        
        //cv::namedWindow("4", cv::WINDOW_NORMAL);
        //cv::imshow("4", src);
    }
    
    //! used to enhance the edge of the goat
    //sharpen(src);
    
    //! additional blur to increase the solidity factor (empiricaly tested)
    cv::blur(src, src, s1);
    
    //cv::namedWindow("5", cv::WINDOW_NORMAL);
    //cv::imshow("5", src);
    
    // add some threshold
    src = src + cv::Scalar::all(10);
}

void saturate(cv::Mat &src)
{
    cv::cvtColor(src, src, CV_RGB2HSV);
    
    #pragma omp parallel for
    for(int y=0;y<src.rows;y++)
    {
        for(int x=0;x<src.cols;x++)
        {
            cv::Vec3b color = src.at<cv::Vec3b>(cv::Point(x,y));
            color[2] = std::max((int)color[2], Config::saturate[0]);
            color[1] = std::max((int)color[1], Config::saturate[1]);
            src.at<cv::Vec3b>(cv::Point(x,y)) = color;
        }
    }
    
    cv::cvtColor(src, src, CV_HSV2RGB);
}

void desaturate(cv::Mat &src)
{
    cv::cvtColor(src, src, CV_RGB2HSV);
    
    #pragma omp parallel for
    for(int y=0;y<src.rows;y++)
    {
        for(int x=0;x<src.cols;x++)
        {
            cv::Vec3b color = src.at<cv::Vec3b>(cv::Point(x,y));
            color[1] = 0;
            src.at<cv::Vec3b>(cv::Point(x,y)) = color;
        }
    }
    
    cv::cvtColor(src, src, CV_HSV2RGB);
}

void value(cv::Mat &src)
{
    cv::cvtColor(src, src, CV_RGB2HSV);
    
    #pragma omp parallel for
    for(int y=0;y<src.rows;y++)
    {
        for(int x=0;x<src.cols;x++)
        {
            cv::Vec3b color = src.at<cv::Vec3b>(cv::Point(x,y));
            color[0] = std::max((int)color[2], 240);
            color[1] = 255;
            src.at<cv::Vec3b>(cv::Point(x,y)) = color;
        }
    }
    
    cv::cvtColor(src, src, CV_HSV2RGB);
}


void equalizeIntensity(Mat& src)
{
    cvtColor(src,src,CV_BGR2YCrCb);

    vector<Mat> channels;
    split(src, channels);

    equalizeHist(channels[0], channels[0]);
    
    merge(channels, src);

    cvtColor(src, src, CV_YCrCb2BGR);
}

void remove_yellow(cv::Mat &src)
{
    cv::cvtColor(src, src, CV_RGB2YCrCb);
    
    #pragma omp parallel for
    for(int y=0;y<src.rows;y++)
    {
        for(int x=0;x<src.cols;x++)
        {
            cv::Vec3b color = src.at<cv::Vec3b>(cv::Point(x,y));
            color[0] = 0;
            src.at<cv::Vec3b>(cv::Point(x,y)) = color;
        }
    }
    
    cv::cvtColor(src, src, CV_YCrCb2RGB);
}

void remove_green_major(cv::Mat &src, int thresh, int max)
{
    #pragma omp parallel for
    for(int y=0;y<src.rows;y++)
    {
        for(int x=0;x<src.cols;x++)
        {
            cv::Vec3b color = src.at<cv::Vec3b>(cv::Point(x,y));
            if(color[1]+thresh > std::max(color[0],color[2]) || color[1] > max)
            {
                color[0] = 255;
                color[1] = 255;
                color[2] = 255;
            }
            src.at<cv::Vec3b>(cv::Point(x,y)) = color;
        }
    }
}


bool is_green_major(const cv::Mat &src, int thresh, int max)
{
    unsigned int r = 0;
    unsigned int g = 0;
    unsigned int b = 0;
    
    #pragma omp parallel for
    for(int y=0;y<src.rows; y+=3)
    {
        for(int x=0;x<src.cols; x+=3)
        {
            cv::Vec3b color = src.at<cv::Vec3b>(cv::Point(x,y));
            r += color[0];
            g += color[1];
            b += color[2];
        }
    }
    
    return g+thresh > std::max(r, b) || g/src.rows/src.cols > max;
}

void sharpen(cv::Mat &src, int enabled_plane)
{
    cv::Mat kernel(3, 3, CV_32F, cv::Scalar(0));
    
    // assigns kernel values
    kernel.at<float>(0,0) = -1.0;
    kernel.at<float>(0,1) = -1.0;
    kernel.at<float>(0,2) = -1.0;
    kernel.at<float>(1,0) = -1.0;
    kernel.at<float>(1,1) =  9.0;
    kernel.at<float>(1,2) = -1.0;
    kernel.at<float>(2,0) = -1.0;
    kernel.at<float>(2,1) = -1.0;
    kernel.at<float>(2,2) = -1.0;

    std::vector<cv::Mat> planes;
    cv::split(src, planes);
    
    #pragma omp parallel for
    for(int i = 0; i<planes.size(); ++i)
        if((enabled_plane >> i) & 0x1)
            cv::filter2D(planes[i], planes[i], planes[i].depth(), kernel);
        
    cv::merge(planes, src);
}