#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "../activity/statistics.h"
#include "../activity/activity.h"

/**
 * usefull function to extract a sub texture from an existing image
 * they get the sub image in the rectangle @rect whiout rotation
 * thay also add a padding to the border of the rectangle size @padding
 * the result is saved to @area
 **/
void extract_area(const cv::Mat &src, const cv::RotatedRect &rect, cv::Mat &area, int padding = 8);

/**
 * this calculate the mean and standar deviation of the given texture
 * this criteria can at least filter some noisy texture (like the weed)
 * @return D > 17.0 && D < 50.0 && M > 100.0 (is empiricaly tested)
 **/
bool filter_by_texture(Statistics &stats, const Activity &src);

/**
 * this fonction calculate the histogram corelation
 * between the given texture @src and all loaded goat in @GoatBank::singleton
 * they select the goat with the best histogram criteria
 * and return true if the distance betwwen the best one and the given texture
 * is enough to be a goat (is empiricaly tested)
 *
 * we have see that in pratice there is no suffisent data to correctly determine
 * if the given texture is a goat, because, goat have in general a size of 40x15 or 20x20
 * this mean that the histogram value have only 600 values witch is not enough
 **/
bool filter_by_histogram(Statistics &stats, const Activity &src);

/**
 * this new filtering methode detect the activity of the contours by assuming this is a goat
 * when the Activity cannot be dertermine this mean that the image is not a goat
 *
 * in other hand when the contous is enough to be detected as a goat
 * we compare the detected contours with @cv::mathContous with all loaded goat in @GoatBank::singleton
 * this function use algorithm that is scale, translation and rotation invariant which is what we want
 * when the contours is close enough (to 0) it's a goat !
 **/
bool filter_by_shape(Statistics &stats, const Activity &src);

/**
 * actually call @filter_by_shape
 *    1 if filter_by_shape     sucess
 *    2 if filter_by_histogram sucess
 *    4 if filter_by_shape     sucess
 * @return a flag integer
 **/
int filter_activity(Statistics &stats, const Activity &src);