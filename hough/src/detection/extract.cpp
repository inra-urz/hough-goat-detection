#include "color.h"
#include "shape.h"

#include "../activity/activity.h"
#include "../activity/check.h"
#include "../tools/bank.h"
#include "../tools/config.h"

#include <iostream>
#include <iomanip>
#include <limits>

void extract_area(const cv::Mat &src, const cv::RotatedRect &rect, cv::Mat &area, int padding)
{
    cv::Rect clipped = rect.boundingRect();
    
    clipped.x -= padding;
    clipped.y -= padding;
    clipped.width += padding;
    clipped.height += padding;
    
    clipped &= cv::Rect(0, 0, src.cols, src.rows);
    
    /*
    rect_size.width = 51;
    rect_size.height = 51;
     */
    
    // crop the resulting image
    cv::getRectSubPix(src, clipped.size(), rect.center, area);
}

bool filter_by_texture(Statistics &stats, const Activity &src)
{
    cv::Scalar mean, dev;
    cv::meanStdDev(src.original, mean, dev);
    
    double M = mean.val[0]; 
    double D = dev.val[0];

    std::cout << M << "--";
    std::cout << D;
    
    return D > Config::min_deviation 
        && D < Config::max_deviation
        && M > Config::min_mean
        && M < Config::max_mean;
}

bool filter_by_histogram(Statistics &stats, const Activity &src)
{
    int histSize = 256;    
    float range[] = { 0, 256 } ;
    const float* histRange = { range };
    
    bool uniform = true;
    bool accumulate = false;
    
    std::array<cv::Mat, 3> hist;
    std::vector<cv::Mat> bgr_planes;
    cv::split(src.original, bgr_planes);

    cv::calcHist(&bgr_planes[0], 1, 0, cv::Mat(), hist[0], 1, &histSize, &histRange, uniform, accumulate);
    cv::calcHist(&bgr_planes[1], 1, 0, cv::Mat(), hist[1], 1, &histSize, &histRange, uniform, accumulate);
    cv::calcHist(&bgr_planes[2], 1, 0, cv::Mat(), hist[2], 1, &histSize, &histRange, uniform, accumulate);
    
    //  CV_COMP_CORREL        Correlation
    //  CV_COMP_CHISQR        Chi-Square
    //  CV_COMP_INTERSECT     Intersection
    //  CV_COMP_BHATTACHARYYA Bhattacharyya distance
    
    double best[3] = {0.0};
    const auto &goat_hist = GoatBank::singleton->getHist();
    
    for(int i = 0; i<goat_hist.size(); ++i)
    {
        best[0] = std::max(best[0], cv::compareHist(goat_hist[i][0], hist[0], CV_COMP_INTERSECT));
        best[1] = std::max(best[1], cv::compareHist(goat_hist[i][1], hist[1], CV_COMP_CORREL));
        best[2] = std::max(best[2], cv::compareHist(goat_hist[i][2], hist[2], CV_COMP_INTERSECT));
    }
    
    // return best[0] > 0.9 && best[1] > 0.7 && best[2] > 0.8; // CV_COMP_CORREL
    return best[0] < Config::hist_correlation[0]// && best[0] < 4000
        && best[1] < Config::hist_correlation[1]
        && best[2] < Config::hist_correlation[2];// && best[2] < 4400;
}

bool filter_by_shape(Statistics &stats, const Activity &src)
{
    float correlation = std::numeric_limits<float>::infinity();
    
    if(src.max_area_index > -1 && src.solidity < Config::max_solidity)
    {
        const auto &references = GoatBank::singleton->getActivity();
        
        for(auto it : references)
        {
            float current = cv::matchShapes(src.contours, it.contours, CV_CONTOURS_MATCH_I3, 10);
            
            /*
            cv::Mat corr;
            cv::matchTemplate(src, it, corr, cv::TM_CCORR_NORMED);
            float current = corr.at<float>(0,0);
            */

            if(it.contours.size() > 0 && current < correlation)
                correlation = current;
        }
    }
    
    if(correlation < Config::shape_correlation)
    {
        std::cout << std::fixed << std::setw(10) << std::right
                  << correlation << " >> ";
        src.print(stats.current_goat);
        
        return true;
    }
    
    return false;
}

int filter_activity(Statistics &stats, const Activity &src)
{
    return !filter_by_shape(stats, src)
        + (!filter_by_texture(stats, src) << 1)
        + (!filter_by_histogram(stats, src) << 2);
}