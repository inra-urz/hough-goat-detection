#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

/**
 * name of the plane created by @create_detection_planes
 **/
static const char *plane_name[] {
    "red",
    "blue",
    "diff",
    "error",
    "error-diff",
    nullptr
};

/**
 * this is the main filtering algorithm
 * use a couple of previous defined function
 * to increase to probability of goat defined pixels
 * they mainly use the Hue of the hsv colorspace
 * @src the input texture
 * @only_detection flag to reduce the calculation time
 * the detection do not need a great accuracy, he only need a blured blob
 * for the ellipse feeting, but they decrease the gaot activity recognition
 **/
void image_processing(cv::Mat &src, bool only_detection = true);

/**
 * this is the second filtering algorithm when the first one failed
 * use a couple of previous defined function
 * to increase to probability of goat defined pixels
 * they try to maximise the definition of the goat hair crest
 * to do that, they mainly use the a blured and normalized desaturated texture
 * the crest appered in black, and the propoagation area is done by blur & bilateral filter
 * @src the input texture
 **/
void error_processing(cv::Mat &src);

/**
 * this create 4 planes (at this time)
 * they use @image_processing and split the result in R,B wich is add to @planes
 * thay also add @error_processing result
 * @src the input texture
 * @planes output texture
 **/
void create_detection_planes(const cv::Mat &src, std::vector<cv::Mat> &planes);

/**
 * change the color space to HSV
 * set saturation to 0
 * then change the colospace to BRG
 * @src the input texture
 **/
void desaturate(cv::Mat &src);

/**
 * change the color space to HSV
 * set saturation and value to 255
 * then change the colospace to BRG
 * @src the input texture
 **/
void saturate(cv::Mat &src);

/**
 * change the color space to HSV
 * set hue and satuation to 255
 * then change the colospace to BRG
 * @src the input texture
 **/
void value(cv::Mat &src);

/**
 * change the color space from BGR to YCrCb
 * set the Yellow plane to 0
 * then change the color space to BGR
 * @src the input texture
 **/
void remove_yellow(cv::Mat &src);

/**
 * set all pixel if BGR when
 * @src the input texture
 * @return G+thresh > std::max(B,R) or G > max, to white color
 **/
void remove_green_major(cv::Mat &src, int thresh = 20, int max = 200);

/**
 * calculate the avarage color of the given texture
 * @return g+thresh > std::max(r, b) || g > max
 **/
bool is_green_major(const cv::Mat &src, int thresh = 20, int max = 200);

/**
 * sharpen all enable plane with a convolution
 * @src the input texture
 * @enabled_plane if enabled_plane = 0x1 | 0x4 and the image is in BGR
 * then only the B and R chanl would be shapend
 **/
void sharpen(cv::Mat &src, int enabled_plane = 0xFF);