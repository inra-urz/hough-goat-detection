#include "shape.h"
#include "../tools/config.h"

#include <iostream>

cv::Mat element_dilate = getErodeDilateMatrix(Config::morphological_erode);
cv::Mat element_erode = getErodeDilateMatrix(Config::morphological_dilate);

float getSurfaceSolidity(const cv::Mat &src, const std::vector<cv::Point> &cnt)
{
    float area, hull_area;
    
    cv::Mat hull, contours(cnt);
    cv::convexHull(contours, hull);
    
    /*
    area = cv::contourArea(cnt);
    hull_area = cv::contourArea(hull);
    */
    
    cv::Mat mask = cv::Mat::zeros(src.size(), CV_8UC1);
    cv::Mat back = cv::Mat::zeros(src.size(), CV_8UC1);
    
    cv::fillConvexPoly(mask, hull, cv::Scalar(255,255,255));
    src.copyTo(back, mask);
    cvtColor(back, back, CV_BGR2GRAY);
    
    area = cv::countNonZero(back);
    hull_area = cv::countNonZero(mask);
    
    float solidity = hull_area / area;
    float optimal = Config::optimal_solidity;
    float length = std::abs(solidity-optimal);
    
    /* the optimal factor has been found by learning
     * this is the solidity factor of the mean of eating goat
     * this is the best factor of the elliptic detection !
     */
    
    return std::sqrt(length*length);
}

cv::Mat getErodeDilateMatrix(unsigned int erosion_size)
{
    unsigned int erosion_type = cv::MORPH_ELLIPSE;
    
    return cv::getStructuringElement(
        erosion_type,
        cv::Size(2*erosion_size+1, 2*erosion_size+1),
        cv::Point(erosion_size, erosion_size)
    );
}

void thresholdAndMorph(
    const cv::Mat &src,
    cv::Mat &threshold_output
  )
{
    cv::Mat src_gray;
    
    if(src.channels() == 3)
        cv::cvtColor(src, src_gray, CV_BGR2GRAY);
    else
        src_gray = src.clone();
    
    cv::threshold(
        src_gray, threshold_output,
        Config::threshold, 255,
        cv::ADAPTIVE_THRESH_GAUSSIAN_C
    );
    
    cv::dilate(threshold_output, threshold_output, element_dilate);
    cv::erode(threshold_output, threshold_output, element_erode);
}

int findAreaAndFitEllipse(
    cv::Mat &threshold_output,
    std::vector<std::vector<cv::Point>> &contours,
    std::vector<cv::RotatedRect> &minEllipse
  )
{
    std::vector<cv::Vec4i> hierarchy;
    std::vector<cv::RotatedRect> minRect;
    
    findContours(threshold_output, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE, cv::Point(0, 0));
    
    minEllipse.resize(contours.size());
    minRect.resize(contours.size());
    
    float max_area_size = 0.0;
    int max_area_index = -1;
    
    for(int i = 0; i<contours.size(); i++)
    {
        minRect[i] = cv::minAreaRect(cv::Mat(contours[i]));
        
        if(contours[i].size() > 20 && max_area_size < contours[i].size())
        {
            minEllipse[i] = cv::fitEllipse(cv::Mat(contours[i]));
            float size = minEllipse[i].size.width * minEllipse[i].size.height;
            if(size > Config::min_contour && size < Config::max_contour)
            {
                max_area_size = contours[i].size();
                max_area_index = i;
            }
        }
    }
    
    return max_area_index;
}

// https://stackoverflow.com/a/30418912/5008845
cv::Rect findMinRect(const cv::Mat1b& src)
{
    cv::Mat1f W(src.rows, src.cols, float(0));
    cv::Mat1f H(src.rows, src.cols, float(0));

    cv::Rect maxRect(0,0,0,0);
    float maxArea = 0.f;

    for(int r = 0; r < src.rows; ++r)
    {
        for(int c = 0; c < src.cols; ++c)
        {
            if(src(r, c) == 0)
            {
                H(r, c) = 1.f + ((r>0) ? H(r-1, c) : 0);
                W(r, c) = 1.f + ((c>0) ? W(r, c-1) : 0);
            }

            float minw = W(r,c);
            
            for(int h = 0; h < H(r, c); ++h)
            {
                minw = std::min(minw, W(r-h, c));
                float area = (h+1) * minw;
                
                if(area > maxArea)
                {
                    maxArea = area;
                    maxRect = cv::Rect(cv::Point(c - minw + 1, r - h), cv::Point(c+1, r+1));
                }
            }
        }
    }

    return maxRect;
}

cv::RotatedRect largestRectInNonConvexPoly(const cv::Mat1b& src)
{
    // Create a matrix big enough to not lose points during rotation
    std::vector<cv::Point> ptz;
    cv::findNonZero(src, ptz);
    
    cv::Rect bbox = boundingRect(ptz); 
    int maxdim = std::max(bbox.width, bbox.height);
    
    cv::Mat1b work(2*maxdim, 2*maxdim, uchar(0));
    src(bbox).copyTo(work(cv::Rect(maxdim - bbox.width/2, maxdim - bbox.height / 2, bbox.width, bbox.height)));

    // Store best data
    cv::Rect bestRect;
    int bestAngle = 0;

    // For each angle
    for(int angle = 0; angle < 90; angle += 1)
    {
        // Rotate the image
        cv::Mat R = cv::getRotationMatrix2D(cv::Point(maxdim,maxdim), angle, 1);
        cv::Mat1b rotated;
        warpAffine(work, rotated, R, work.size());

        // Keep the crop with the polygon
        std::vector<cv::Point> pts;
        cv::findNonZero(rotated, pts);
        cv::Rect box = boundingRect(pts);
        cv::Mat1b crop = rotated(box).clone();

        // Invert colors
        crop = ~crop; 

        // Solve the problem: "Find largest rectangle containing only zeros in an binary matrix"
        // https://stackoverflow.com/questions/2478447/find-largest-rectangle-containing-only-zeros-in-an-n%C3%97n-binary-matrix
        cv::Rect r = findMinRect(crop);

        // If best, save result
        if(r.area() > bestRect.area())
        {
            bestRect = r + box.tl();    // Correct the crop displacement
            bestAngle = angle;
        }
    }

    // Apply the inverse rotation
    cv::Mat Rinv = getRotationMatrix2D(cv::Point(maxdim, maxdim), -bestAngle, 1);
    
    std::vector<cv::Point> rectPoints{
        bestRect.tl(),
        cv::Point(bestRect.x + bestRect.width, bestRect.y),
        bestRect.br(),
        cv::Point(bestRect.x, bestRect.y + bestRect.height)
    };
    
    std::vector<cv::Point> rotatedRectPoints;
    cv::transform(rectPoints, rotatedRectPoints, Rinv);

    // Apply the reverse translations
    for(int i = 0; i < rotatedRectPoints.size(); ++i)
        rotatedRectPoints[i] += bbox.tl() - cv::Point(maxdim - bbox.width / 2, maxdim - bbox.height / 2);

    // Get the rotated rect
    cv::RotatedRect rrect = minAreaRect(rotatedRectPoints);

    return rrect;
}

void drawAxis(cv::Mat& img, cv::Point p, cv::Point q, cv::Scalar colour, const float scale)
{
    double angle;
    double hypotenuse;
    
    // angle in radians
    angle = atan2( (double) p.y - q.y, (double) p.x - q.x );
    hypotenuse = sqrt( (double) (p.y - q.y) * (p.y - q.y) + (p.x - q.x) * (p.x - q.x));
    
//    double degrees = angle * 180 / CV_PI; // convert radians to degrees (0-180 range)
//    cout << "Degrees: " << abs(degrees - 180) << endl; // angle in 0-360 degrees range

    // Here we lengthen the arrow by a factor of scale
    q.x = (int) (p.x - scale * hypotenuse * cos(angle));
    q.y = (int) (p.y - scale * hypotenuse * sin(angle));
    line(img, p, q, colour, 1, cv::LINE_AA);
    
    // create the arrow hooks
    p.x = (int) (q.x + 9 * cos(angle + CV_PI / 4));
    p.y = (int) (q.y + 9 * sin(angle + CV_PI / 4));
    line(img, p, q, colour, 1, cv::LINE_AA);
    
    p.x = (int) (q.x + 9 * cos(angle - CV_PI / 4));
    p.y = (int) (q.y + 9 * sin(angle - CV_PI / 4));
    line(img, p, q, colour, 1, cv::LINE_AA);
}

float getOrientation(const std::vector<cv::Point> &pts, cv::Mat &img)
{
    //Construct a buffer used by the pca analysis
    int sz = static_cast<int>(pts.size());
    cv::Mat data_pts = cv::Mat(sz, 2, CV_64FC1);
    
    for(int i = 0; i < data_pts.rows; ++i)
    {
        data_pts.at<double>(i, 0) = pts[i].x;
        data_pts.at<double>(i, 1) = pts[i].y;
    }
    
    //Perform PCA analysis
    cv::PCA pca_analysis(data_pts, cv::Mat(), cv::PCA::DATA_AS_ROW);
    
    //Store the center of the object
    cv::Point cntr = cv::Point(
        static_cast<int>(pca_analysis.mean.at<double>(0, 0)),
        static_cast<int>(pca_analysis.mean.at<double>(0, 1))
    );
                      
    //Store the eigenvalues and eigenvectors
    std::vector<cv::Point2d> eigen_vecs(2);
    std::vector<double> eigen_val(2);
    
    for(int i = 0; i < 2; ++i)
    {
        eigen_vecs[i] = cv::Point2d(
            pca_analysis.eigenvectors.at<double>(i, 0),
            pca_analysis.eigenvectors.at<double>(i, 1)
        );
        eigen_val[i] = pca_analysis.eigenvalues.at<double>(i);
    }
    
    // Draw the principal components
    // cv::circle(img, cntr, 3, cv::Scalar(255, 0, 255), 2);
    
    cv::Point p1 = cntr + 0.02 * cv::Point(eigen_vecs[0].x * eigen_val[0], eigen_vecs[0].y * eigen_val[0]);
    cv::Point p2 = cntr - 0.02 * cv::Point(eigen_vecs[1].x * eigen_val[1], eigen_vecs[1].y * eigen_val[1]);
    
    drawAxis(img, cntr, p1, cv::Scalar(0, 255, 0), 1);
    drawAxis(img, cntr, p2, cv::Scalar(0, 0, 255), 5);
    
    // orientation in radians
    return atan2(eigen_vecs[0].y, eigen_vecs[0].x);
}

std::pair<extrem_point, extrem_point> getExtremPoint(const std::vector<cv::Point> &contours)
{
    auto width = std::minmax_element(
        contours.begin(),
        contours.end(),
        [](cv::Point const& a, cv::Point const& b){
            return a.x < b.x;
        }
    );
    
    auto height = std::minmax_element(
        contours.begin(),
        contours.end(),
        [](cv::Point const& a, cv::Point const& b){
            return a.y < b.y;
        }
    );
    
    return std::pair<extrem_point, extrem_point>(
        extrem_point(*width.first,  *width.second),
        extrem_point(*height.first, *height.second)
    );
}