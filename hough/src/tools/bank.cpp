#include "bank.h"
#include "../activity/check.h"
#include "../activity/statistics.h"

#include <experimental/filesystem>
#include <algorithm>
#include <iostream>

using namespace std::experimental::filesystem;

GoatBank* GoatBank::singleton = nullptr;

GoatBank::GoatBank()
    : max_name_size(0)
{
}

void GoatBank::add_goat(const std::string &filename)
{
    int histSize = 256;    
    float range[] = { 0, 256 } ;
    const float* histRange = { range };
    
    bool uniform = true;
    bool accumulate = false;
    
    cv::Mat src = cv::imread(filename.c_str(), 1);
    
    std::array<cv::Mat, 3> histogram;
    std::vector<cv::Mat> bgr_planes;
    split(src, bgr_planes);
    
    if(bgr_planes.size() != 3)
        return;

    cv::calcHist(&bgr_planes[0], 1, 0, cv::Mat(), histogram[0], 1, &histSize, &histRange, uniform, accumulate);
    cv::calcHist(&bgr_planes[1], 1, 0, cv::Mat(), histogram[1], 1, &histSize, &histRange, uniform, accumulate);
    cv::calcHist(&bgr_planes[2], 1, 0, cv::Mat(), histogram[2], 1, &histSize, &histRange, uniform, accumulate);

    if(filename.find("_D") != std::string::npos)
        state.push_back(0);
    else if(filename.find("_C") != std::string::npos)
        state.push_back(1);
    else
        state.push_back(std::string::npos);
        
    if(max_name_size < filename.size())
        max_name_size = filename.size();
        
    image.push_back(src);
    hist.push_back(histogram);
    name.push_back(filename);
    
    activities.push_back(Activity());
}

Statistics GoatBank::initialize_activities(bool nogui, bool write, bool noout)
{
    Statistics stats;
    
    for(int i = 0; i<size(); ++i)
    {
        stats.current_goat = i;
    
        activities[i] = split_and_check_activity(
            stats,    image[i],
            state[i], name[i].c_str(),
            nogui,    write,
            noout
        );
        
        if(!nogui)
        {
            cv::namedWindow("original", cv::WINDOW_NORMAL);
            cv::imshow("original", image[i]);
            cv::waitKey(0);
        }
    }
    
    return stats;
}

void GoatBank::load_dir(const std::string &dir)
{
    std::vector<std::string> filenames;
    
    for(auto &p: directory_iterator(dir))
    {
        std::string filename = dir + "/";
                    filename += p.path().filename();
        
        if(is_directory(p.path()))
            load_dir(filename);
        else
            filenames.push_back(filename);
    }
    
    std::sort(filenames.begin(), filenames.end());
    
    for(auto &filename: filenames)
        add_goat(filename);
}