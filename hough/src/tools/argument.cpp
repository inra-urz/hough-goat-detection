#include "argument.h"
#include "config.h"

#include <iostream>

ArgumentParsing* ArgumentParsing::singleton = nullptr;

ArgumentParsing::ArgumentParsing(int argc, char** argv, bool detection)
    : nogui(false), csv(false)
    , write(false), noout(false)
    , detection(detection)
{
    bool dir = false;
    bool have_config = false;

    for(int i=1; i<argc; ++i)
    {
        if(argv[i][0] == '-' && argv[i][1] == '-')
        {
            if(std::string("--help") == argv[i])
                help();
            
            noout |= std::string("--noout") == argv[i];
            nogui |= std::string("--nogui") == argv[i];
            write |= std::string("--write") == argv[i];
            csv   |= std::string("--csv")   == argv[i];
                
            if(std::string("--dir") == argv[i] && i+1<argc)
            {
                GoatBank::singleton->load_dir(argv[i+1]);
                dir = true;
                i++;
            }
                
            if(std::string("--config") == argv[i] && i+1<argc)
            {
                Config::read(argv[i+1]);
                have_config = true;
                i++;
            }
        }
        else
            filenames.push_back(argv[i]);
    }
    
    if(!have_config)
        Config::read();
    
    if(detection == false)
    {
        if(filenames.size() == 0 && !dir)
        {
            GoatBank::singleton->load_dir("set/learning/Black");
            GoatBank::singleton->load_dir("set/learning/Red");
        }
        
        for(auto filename : filenames)
            GoatBank::singleton->add_goat(filename);
    }
    else if(!dir)
    {
        GoatBank::singleton->load_dir("set/learning/Black");
        GoatBank::singleton->load_dir("set/learning/Red");
    }
}

void ArgumentParsing::help() const noexcept
{
    std::cout
        << "--nogui disable graphical interface \n"
        << "--csv   output the detected goat as csv format \n"
        << "--write save the detected goat/plane into the ./area/ folder \n"
        << "--dir   load goat bank from specific folder (recursive) \n"
        << "other argument to load specific goats or pasture"
        << std::endl;
    exit(0);
}