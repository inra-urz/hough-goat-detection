#pragma once

#include "../activity/activity.h"
#include "../activity/statistics.h"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <string>
#include <vector>
#include <array>

/**
 * a tiny struct of reference to access to an individual goat information
 **/
struct GoatBankRef
{
    std::string &name;
    size_t  &state;
    cv::Mat &image;
    std::array<cv::Mat, 3> &hist;
};

/**
 * a usefull tool to load goat from a folder and store the loaded Matrix and Historgram and other properties
 * would have a unique instance @GoatBank::singleton
 **/
class GoatBank
{
    public:
        static GoatBank *singleton;
    public:
        //! create a new empty bank
        GoatBank();
        
        //! load and add a new goat to the bank
        void add_goat(const std::string &name);
        //! recursively add all goat texture for the give folder @name
        void load_dir(const std::string &name);

        //! return all loaded goat texture
        inline const std::vector<cv::Mat>& getBank() const noexcept                { return image; }
        //! return all goat filename
        inline const std::vector<std::string>& getName() const noexcept            { return name;  }
        //! return all goat dectected state from the filename (_D or _C)
        inline const std::vector<size_t>& getState() const noexcept                { return state; }
        //! return all goat 3 plane histogram
        inline const std::vector<std::array<cv::Mat, 3>>& getHist() const noexcept { return hist;  }
        //! return all goat actitivity
        inline const std::vector<Activity>& getActivity() const noexcept           { return activities; }
        
        //! goat iterator begin
        inline GoatBankRef begin() noexcept { return (*this)[0];        }
        //! goat iterator end
        inline GoatBankRef end() noexcept   { return (*this)[size()-1]; }
        
        //! get a structure reference of a goat at the index @i, overflow and underflow untested
        GoatBankRef operator[] (unsigned int i) noexcept {
            return GoatBankRef{ name[i], state[i], image[i], hist[i] };
        }
        
        //! return the number of loaded goat
        inline unsigned int size() const noexcept { return name.size(); }
        
        inline unsigned int name_size() const noexcept { return max_name_size; }
        
        Statistics initialize_activities(bool nogui = true, bool write = false, bool noout = false);
        
    protected:
        //! all goat filenames
        std::vector<std::string> name;
        //! all goat name_state
        std::vector<size_t> state;
        //! all goat original image
        std::vector<cv::Mat> image;
        //! all goat histogram of the original image
        std::vector<std::array<cv::Mat, 3>> hist;
        //! all goat activity
        std::vector<Activity> activities;
        //! maximum length of all goat name in @name
        unsigned int max_name_size;
    private:
        //! calculate and fill all goat histogram
        void transform_histogram();
};