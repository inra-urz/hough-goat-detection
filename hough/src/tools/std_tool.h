#pragma once

#include <string>
#include <string_view>
#include <sstream>
#include <vector>

namespace std
{
    inline vector<string_view> split(const string_view &data, char delim)
    {
        vector<string_view> buf;
        
        auto i = 0;
        auto pos = data.find(delim);
        
        while(pos != string::npos)
        {
            if(pos-i > 0)
                buf.push_back(data.substr(i, pos-i));
            i = ++pos;
            pos = data.find(delim, pos);
        }
        
        if(data.length()-i > 0)
            buf.push_back(data.substr(i, data.length()));
        
        return move(buf);
    }

    inline void from_string(const string& s, int &result)
    {
       result = std::atoi(s.c_str());
    }

    inline void from_string(const string& s, float &result)
    {
       result = std::atof(s.c_str());
    }

    inline void from_string(const string& s, double &result)
    {
       result = std::atof(s.c_str());
    }
    
    template <typename T, unsigned k>
    inline void ar_from_string(const string& s, T *result)
    {
        auto values = split(s, ',');
        auto size = min((unsigned)values.size(), k);
        for(int i = 0; i<size; ++i)
        {
            string d(values[i].data(), values[i].size());
            from_string(d, result[i]);
        }
    }

    template<typename T, unsigned k>
    inline string ar_to_string(T d[k])
    {
        string tmp = to_string(d[0]);
        for(int i = 1; i<k; ++i)
            tmp += "," + to_string(d[i]);
        return tmp;
    }
}