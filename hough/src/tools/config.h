#pragma once

#include "trackbar.h"

#include <string>
#include <memory>

class Config
{
    public:
        static std::string filename;
    public:
        static void write(const std::string &filename = "config.cfg");
        static void read(const std::string &filename = "config.cfg");
        static void sliders(cv::TrackbarCallback, void*);
        static void print();
    public:
        // detection/color.cpp
        static int sharpen;
        static int bilateral;
        static int remove_yellow;
        static int blur_size[2];
        static int remove_green_threshold[2];
        static int remove_green_max[2];
        static int saturate[2];
    public:
        // detection/shape.cpp
        static int threshold;
        static int min_contour;
        static int max_contour;
        static int morphological_erode;
        static int morphological_dilate;
        static double optimal_solidity;
    public:
        // detection/extract.cpp
        static double shape_correlation;
        static double hist_correlation[3];
        static double max_solidity;
        static double min_deviation;
        static double max_deviation;
        static double min_mean;
        static double max_mean;
    public:
        // activity/check.cpp
        static double hyperplane[18];
    private:
        static std::vector<std::shared_ptr<ShadowTrack>> tracked;
};
