#pragma once

#include "opencv2/highgui/highgui.hpp"

#include <string>

class ShadowTrack
{
    public:
        int value;
        double precision;
        void *userdata;
        void(*callback)(int, void*);
};

template<typename T>
class GenericTrack : public ShadowTrack
{
    public:
        GenericTrack(const std::string& field_name, const std::string& window, T *current, T max_value, void(*function)(int, void*), void *user, T p = 100)
        {
            data = current;
            userdata = user;
            value = *current * p;
            callback = function;
            precision = p;
            
            cv::createTrackbar(field_name, window, &value, max_value * precision, GenericTrack<T>::callback_proxy, this);
        }

        static void callback_proxy(int, void* object)
        {
            GenericTrack* pObject = static_cast<GenericTrack<T>*>(object);
            *pObject->data = pObject->value / pObject->precision;
            pObject->callback(pObject->value, pObject->userdata);
        }
    public:
        T *data;
};