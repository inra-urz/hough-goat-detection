#pragma once

#include "bank.h"

/**
 * a usefull tool to manage the input programme argument
 * parse the argument to fill the class properties
 * would have a unique instance @ArgumentParsing::singleton
 **/
class ArgumentParsing
{
    public:
        static ArgumentParsing *singleton;
    public:
        /**
         * parse the programme argument and fill the properties of this class singleton
         * @argc @see main::argc
         * @argv @see main::argv
         * @detection true for main_detection and false for main_activity
         */
        ArgumentParsing(int argc, char** argv, bool  = false);
        
        void help() const noexcept;
    public:
        // enable plane write to the "area" folder
        bool write;
        // disable rendering
        bool nogui;
        // output as csv format
        bool csv;
        // disable console output execpt the statistics
        bool noout;
        // tell if it's activity(false) or detection(true) program
        bool detection;
        // filenames of goat to be loaded
        std::vector<const char*> filenames;
};