#include "config.h"

#include "std_tool.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <ios>

std::string Config::filename = "config.cfg";
std::vector<std::shared_ptr<ShadowTrack>> Config::tracked;

// detection/color.cpp
int Config::sharpen = true;
int Config::bilateral = true;
int Config::remove_yellow = true;
int Config::blur_size[] = {2, 8};
int Config::remove_green_threshold[2] = {-5, 5};
int Config::remove_green_max[2] = {100, 110};
int Config::saturate[2] = {240, 240};

// detection/shape.cpp
int Config::threshold = 200;
int Config::min_contour = 100;
int Config::max_contour = 1000;
int Config::morphological_erode = 3;
int Config::morphological_dilate = 3;
double Config::optimal_solidity = 1.1166999614f;

// detection/extract.cpp
double Config::shape_correlation = 0.4;
double Config::hist_correlation[] = {1300, 0.98, 1300};
double Config::max_solidity = 0.6;
double Config::min_deviation = 17.0;
double Config::max_deviation = 50.0;
double Config::min_mean = 100.0;
double Config::max_mean = 1000.0;

// activity/check.cpp
double Config::hyperplane[] = {
    0.06696595572522847, 0.0, 0.0, 0.0,
    0.03363893917701237, 0.0, 0.0, 0.0,
    0.0,                 0.0, 0.0, 0.0, 0.0,
    0.06696595572522847, 0.0,
    0.07431067440843914, 0.0,
    -0.1337815784958322
};

void Config::read(const std::string &filename)
{
    std::unordered_map<std::string, std::string> options;
    std::ifstream cfgfile(filename);
    
    if(!cfgfile.is_open())
    {
        std::cerr << ">> cannot open " << filename << " configuration file" << std::endl;
        std::cerr << "<< write and use default setting" << std::endl;
        write(filename);
        return;
    }
    
    for(std::string line; std::getline(cfgfile, line);)
    {
        std::istringstream iss(line);
        std::string id, eq, val;

        bool error = false;

        if (!(iss >> id))
        {
            error = true;
        }
        else if (id[0] == '#')
        {
            continue;
        }
        else if (!(iss >> eq >> val >> std::ws) || eq != "=" || iss.get() != EOF)
        {
            error = true;
        }

        if(error)
        {
            std::cerr << "config invalid syntaxe:" << std::endl
                      << "\t" << line << std::endl;
            // do something appropriate: throw, skip, warn, etc.
        }
        else
        {
            options[id] = val;
        }
    }
    
    for(auto it : options)
    {
        if(it.first == "sharpen")
            std::from_string(it.second, Config::sharpen);
        else if(it.first == "bilateral")
            std::from_string(it.second, Config::bilateral);
        else if(it.first == "remove_yellow")
            std::from_string(it.second, Config::remove_yellow);
        else if(it.first == "blur_size")
            std::ar_from_string<int,2>(it.second, Config::blur_size);
        else if(it.first == "remove_green_threshold")
            std::ar_from_string<int,2>(it.second, Config::remove_green_threshold);
        else if(it.first == "remove_green_max")
            std::ar_from_string<int,2>(it.second, Config::remove_green_max);
        else if(it.first == "saturate")
            std::ar_from_string<int,2>(it.second, Config::saturate);
            
            
        else if(it.first == "threshold")
            std::from_string(it.second, Config::threshold);
        else if(it.first == "min_contour")
            std::from_string(it.second, Config::min_contour);
        else if(it.first == "max_contour")
            std::from_string(it.second, Config::max_contour);
        else if(it.first == "morphological_erode")
            std::from_string(it.second, Config::morphological_erode);
        else if(it.first == "morphological_dilate")
            std::from_string(it.second, Config::morphological_dilate);
        else if(it.first == "optimal_solidity")
            std::from_string(it.second, Config::optimal_solidity);
            
            
        else if(it.first == "shape_correlation")
            std::from_string(it.second, Config::shape_correlation);
        else if(it.first == "hist_correlation")
            std::ar_from_string<double,3>(it.second, Config::hist_correlation);
        else if(it.first == "max_solidity")
            std::from_string(it.second, Config::max_solidity);
        else if(it.first == "min_deviation")
            std::from_string(it.second, Config::min_deviation);
        else if(it.first == "max_deviation")
            std::from_string(it.second, Config::max_deviation);
        else if(it.first == "min_mean")
            std::from_string(it.second, Config::min_mean);
        else if(it.first == "max_mean")
            std::from_string(it.second, Config::max_mean);
        
        else if(it.first == "hyperplane")
            std::ar_from_string<double,18>(it.second, Config::hyperplane);
        else
            std::cerr << "bad paramaters id : " << it.first << std::endl;
    }
    
    Config::filename = filename;
    print();
}

std::unordered_map<std::string, std::string> fill_parameters()
{
    std::unordered_map<std::string, std::string> options;
    
    options["sharpen"] = std::to_string(Config::sharpen);
    options["bilateral"] = std::to_string(Config::bilateral);
    options["remove_yellow"] = std::to_string(Config::remove_yellow);
    options["blur_size"] = std::ar_to_string<int, 2>(Config::blur_size);
    options["remove_green_threshold"] = std::ar_to_string<int, 2>(Config::remove_green_threshold);
    options["remove_green_max"] = std::ar_to_string<int, 2>(Config::remove_green_max);
    options["saturate"] = std::ar_to_string<int, 2>(Config::saturate);
    
    options["threshold"] = std::to_string(Config::threshold);
    options["optimal_solidity"] = std::to_string(Config::optimal_solidity);
    options["min_contour"] = std::to_string(Config::min_contour);
    options["max_contour"] = std::to_string(Config::max_contour);
    options["morphological_erode"] = std::to_string(Config::morphological_erode);
    options["morphological_dilate"] = std::to_string(Config::morphological_dilate);
    
    options["shape_correlation"] = std::to_string(Config::shape_correlation);
    options["hist_correlation"] = std::ar_to_string<double, 3>(Config::hist_correlation);
    options["max_solidity"] = std::to_string(Config::max_solidity);
    options["min_deviation"] = std::to_string(Config::min_deviation);
    options["max_deviation"] = std::to_string(Config::max_deviation);
    options["min_mean"] = std::to_string(Config::min_mean);
    options["max_mean"] = std::to_string(Config::max_mean);
    
    options["hyperplane"] = std::ar_to_string<double, 18>(Config::hyperplane);
    
    return options;
}

void Config::write(const std::string &filename)
{
    std::unordered_map<std::string, std::string> options = fill_parameters();
    
    std::ofstream cfgfile(filename, std::ios::binary);
    
    if(!cfgfile.is_open())
        throw std::ios_base::failure("error while writing the configuration file");
        
    for(auto it : options)
    {
        cfgfile << it.first;
        cfgfile << " = ";
        cfgfile << it.second;
        cfgfile << std::endl;
    }
    
    cfgfile.flush();
    
    Config::filename = filename;
}

void Config::print()
{
    std::unordered_map<std::string, std::string> options = fill_parameters();
        
    for(auto it : options)
    {
        std::cout << "    >> " << it.first;
        std::cout << " = ";
        std::cout << it.second;
        std::cout << std::endl;
    }
}

void Config::sliders(cv::TrackbarCallback callback, void *user)
{
    cv::namedWindow("ConfigColor", cv::WINDOW_NORMAL);
    cv::namedWindow("ConfigShape", cv::WINDOW_NORMAL);
    cv::namedWindow("ConfigExtract", cv::WINDOW_NORMAL);
    
    Config::tracked.clear();
    
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("sharpen",                  "ConfigColor", &Config::sharpen, 1, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("bilateral",                "ConfigColor", &Config::bilateral, 1, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("remove_yellow",            "ConfigColor", &Config::remove_yellow, 1, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("blur_size_0",              "ConfigColor", &Config::blur_size[0], 10, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("blur_size_1",              "ConfigColor", &Config::blur_size[1], 10, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("remove_green_threshold_0", "ConfigColor", &Config::remove_green_threshold[0], 255, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("remove_green_threshold_1", "ConfigColor", &Config::remove_green_threshold[1], 255, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("remove_green_max_0",       "ConfigColor", &Config::remove_green_max[0], 255, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("remove_green_max_1",       "ConfigColor", &Config::remove_green_max[1], 255, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("saturate_0",               "ConfigColor", &Config::saturate[0], 255, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("saturate_1",               "ConfigColor", &Config::saturate[1], 255, callback, user, 1));
    
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("threshold",                "ConfigShape", &Config::threshold, 255, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("min_contour",              "ConfigShape", &Config::min_contour, 2000, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("max_contour",              "ConfigShape", &Config::max_contour, 2000, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("morphological_erode",      "ConfigShape", &Config::morphological_erode, 50, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<int>>("morphological_dilate",     "ConfigShape", &Config::morphological_dilate, 50, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<double>>("optimal_solidity",      "ConfigShape", &Config::optimal_solidity, 2, callback, user));
      
    Config::tracked.push_back(std::make_shared<GenericTrack<double>>("shape_correlation",   "ConfigExtract", &Config::shape_correlation, 2, callback, user));
    Config::tracked.push_back(std::make_shared<GenericTrack<double>>("hist_correlation_0",  "ConfigExtract", &Config::hist_correlation[0], 2000, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<double>>("hist_correlation_1",  "ConfigExtract", &Config::hist_correlation[1], 1, callback, user, 100));
    Config::tracked.push_back(std::make_shared<GenericTrack<double>>("hist_correlation_2",  "ConfigExtract", &Config::hist_correlation[2], 2000, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<double>>("max_solidity",        "ConfigExtract", &Config::max_solidity, 1, callback, user));
    Config::tracked.push_back(std::make_shared<GenericTrack<double>>("min_deviation",       "ConfigExtract", &Config::min_deviation, 100, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<double>>("max_deviation",       "ConfigExtract", &Config::max_deviation, 100, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<double>>("min_mean",            "ConfigExtract", &Config::min_mean, 1000, callback, user, 1));
    Config::tracked.push_back(std::make_shared<GenericTrack<double>>("max_mean",            "ConfigExtract", &Config::max_mean, 1000, callback, user, 1));
}