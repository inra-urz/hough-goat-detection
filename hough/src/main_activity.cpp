#include "activity/check.h"
#include "detection/shape.h"
#include "tools/argument.h"
#include "tools/config.h"

#include <iostream>

/** @function main */
int main(int argc, char** argv)
{
    GoatBank::singleton = new GoatBank();
    ArgumentParsing::singleton = new ArgumentParsing(argc, argv);
    Activity::print_header();
    
    element_dilate = getErodeDilateMatrix(Config::morphological_erode);
    element_erode = getErodeDilateMatrix(Config::morphological_dilate);
    
    auto stats = GoatBank::singleton->initialize_activities(
        ArgumentParsing::singleton->nogui,
        ArgumentParsing::singleton->write,
        ArgumentParsing::singleton->noout
    );
    
    stats.print();
    
    return 0;
}
