#include "detection/color.h"
#include "detection/extract.h"
#include "detection/shape.h"
#include "activity/check.h"
#include "tools/argument.h"
#include "tools/config.h"

#include <iostream>

static const cv::Scalar background[3] = {
    cv::Scalar(255,  0,255),  // good
    cv::Scalar(255,  0,  0),  // bad
    cv::Scalar(  0,  0,255)   // error
};
static const cv::Scalar foreground[3] = {
    cv::Scalar(255,  0,255),  // good
    cv::Scalar(255,255,255),  // bad
    cv::Scalar(  0,  0,255)   // error
};
        
/**
 * the main algorithm to detect goat form a plane !
 **/
void detect_goat(Statistics &stats, const cv::Mat &original, const cv::Mat &src);

struct DetectionInfos
{
    Statistics stats;
    cv::Mat original;
    cv::Mat src;
};

void update(int, void *userdata)
{
    DetectionInfos *object = static_cast<DetectionInfos*>(userdata);
    
    object->src = object->original.clone();
    image_processing(object->src, true);
    
    cv::namedWindow("Source", cv::WINDOW_NORMAL);
    cv::imshow("Source", object->src);
    
    element_dilate = getErodeDilateMatrix(Config::morphological_erode);
    element_erode = getErodeDilateMatrix(Config::morphological_dilate);

    detect_goat(object->stats, object->original, object->src);
    
    Config::write(Config::filename);
}

int main(int argc, char** argv)
{
    std::cout << "initializing parameters ..." << std::endl;
    
    GoatBank::singleton = new GoatBank();
    ArgumentParsing::singleton = new ArgumentParsing(argc, argv, true);
    
    std::cout << "initializing goats activities ..." << std::endl;
    
    auto stats = GoatBank::singleton->initialize_activities(true, false, true);
    
    std::cout << "    sleeping database : " << stats.true_positive + stats.false_positive << std::endl;
    std::cout << "    eating database   : " << stats.true_negative + stats.false_negative << std::endl;
    
    DetectionInfos infos;
      Config::sliders(update, &infos);
    
    for(auto &it : ArgumentParsing::singleton->filenames)
    {
        std::cout << "detecting goat for " << it << ":" << std::endl;
        
        infos.original = cv::imread(it, 1);
        update(0, &infos);

        cv::waitKey(0);
    }
    
    infos.stats.print();
    
    return(0);
}

void detect_goat(Statistics &stats, const cv::Mat &original, const cv::Mat &src)
{
    cv::Mat src_gray;
    cv::Mat threshold_output;

    cv::cvtColor(src, src_gray, CV_BGR2GRAY);
    
		thresholdAndMorph(src_gray, threshold_output);
    
    cv::namedWindow("Threshold", cv::WINDOW_NORMAL);
    cv::imshow("Threshold", threshold_output);
    
    std::cout << "findContours" << std::endl;
    
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::Vec4i> hierarchy;
    
    cv::findContours(
        threshold_output, contours, hierarchy,
        cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE, cv::Point(0, 0)
    );

    /// Find the rotated rectangles and ellipses for each contour
    std::vector<cv::RotatedRect> minRect(contours.size());
    std::vector<cv::RotatedRect> minEllipse(contours.size());
    std::vector<bool>            enabled(contours.size());

    std::cout << "filtering ... " << std::endl;
    unsigned int goat_count = 0;
    
    #pragma omp parallel for
    for(int i = 0; i < contours.size(); i++)
    {
        minRect[i] = cv::minAreaRect(cv::Mat(contours[i]));
        enabled[i] = false;
        
        if(contours[i].size() > 5 && contours[i].size() < 300)
        {
            minEllipse[i] = cv::fitEllipse(cv::Mat(contours[i]));
            
            float size = minEllipse[i].size.width * minEllipse[i].size.height;
            float aspect = std::min(minEllipse[i].size.width, minEllipse[i].size.height)
                         / std::max(minEllipse[i].size.width, minEllipse[i].size.height);
            
            enabled[i] = aspect > 0.2 && size > 7*7 && size < 2000;
        }
    }

    /// Draw contours + rotated rects + ellipses
    cv::Mat drawing = cv::Mat::zeros(threshold_output.size(), CV_8UC3);
    cv::Mat texture;
    
    Activity::print_header();
    
    #pragma omp parallel for
    for(int i = 0; i< contours.size(); i++)
    {
        stats.current_goat = goat_count;
        
        if(!enabled[i])
        {
            cv::ellipse(drawing, minEllipse[i], background[2], 2, 8);
            continue;
        }
        
        extract_area(original, minEllipse[i], texture);
    
        Activity src = split_and_check_activity(
            stats, texture, -1,
            std::to_string(stats.current_goat).c_str(),
            true, false, true
        );
        
        src.x = minEllipse[i].center.x;
        src.y = minEllipse[i].center.y;
        src.name = std::to_string(i);
        
        int isgoat = filter_activity(stats, src);
        
        if(isgoat == 0)
        {
            if(ArgumentParsing::singleton->write)
            {
                std::string filename = std::string("area/") + src.name + ".jpg";
                cv::imwrite(filename.c_str(), src.original);
            }
            
            goat_count++;
        }
        
        cv::ellipse(drawing, minEllipse[i], background[isgoat == 0], 2, 8);
        
        std::string display = std::to_string(isgoat);
        display +=  "-";
        display +=  src.name;
        
        cv::putText(
            drawing, display.c_str(), minEllipse[i].center,
            cv::FONT_HERSHEY_COMPLEX_SMALL,
            1.0, foreground[isgoat == 0], 1, CV_AA
        );
    }
    
    std::cout << "detected goats : " << goat_count << std::endl;
   
    cv::Mat dst;
    double alpha = 0.5;
    double beta =(1.0 - alpha);
    cv::addWeighted(original, alpha, drawing, beta, 0.0, dst);

    /// Show in a window
    cv::namedWindow("Contours", cv::WINDOW_NORMAL);
    cv::imshow("Contours", dst);
}