#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

/**
 * possible state of an Activity
 * this is related to the properties of the class @Activity
 * @Activity::name_state and @Activity::detected_state
 **/
enum E_DETECTED_STATE
{
    EDS_SLEEPING,
    EDS_EATING,
    EDS_ERROR
};

/**
 * name of the possible Activity state
 * @Activity::name_state and @Activity::detected_state
 **/
static const char *e_detected_state_name[] =
{
    "sleeping",
    "eating",
    "error",
    nullptr
};

/**
 * an instance of this class represent of possible Activity detection
 * from the function @check_activity, they also compute the maximal goat detection
 * the function @check_activity set major properties expect the @detected_state
 * form an array of Activity (the best one) of the function @split_and_check_activity
 * the function @split_and_check_activity set the final @detected_state
 **/
class Activity
{
    public:
        /**
         * set all properties to zero
         * except detected_state = 2
         * except max_area_index = -1
         * except selected = -1
         **/
        Activity();
        
        /**
         * load an activity from existing plane
         * @src is the input plane @see code of the function split_and_check_activity
         * @plane_name is the name displayed if "--nogui" is unset @see color.h::plane_name
         **/
        Activity(const cv::Mat &src, const char *plane_name);
        
        /**
         * set this Activity by selecting the best one in @activities
         * the main criteria is the solidity factor
         * we suppose that the solidity factor select the region
         * who have the best detection and contrast
         **/
        Activity(const std::vector<Activity> &activities);
        
        /**
         * print the right header depending of the program argument --csv
         **/
        static void print_header();
        
        /**
         * print the normal header
         * filename  --  aspect    state     status solidity  --  selected
         **/
        static void print_shell_header();
        
        /**
         * print the csv header for column name
         * name;area;aspect;state;status;solidity;extent;equivalent_diameter;orientation
         **/
        static void print_csv_header();
        
        /**
         * print the goat activity properties depending of the actual program argument --csv
         **/
        void print(int current_goat) const noexcept;
        
        /**
         * print the most usefull properties in the shell like design
         * @see print_shell_header
         **/
        void print_shell(int current_goat) const noexcept;
        
        /**
         * print all properties
         * @see print_csv_header
         **/
        void print_csv(int current_goat) const noexcept;
        
        /**
         * use only when the detection is done
         * the function @check_activity set the @detected_state variable
         * the function @split_and_check_activity set the @name_state variable
         * @return name_state == detected_state
         **/
        inline int is_correct() const noexcept {
            if(name_state == EDS_ERROR)
                return EDS_ERROR;
            else
                return name_state == detected_state;
        }
    
    public:
        // RotatedRect properties
        float width, height;
        float x, y;
        
        // the original filename
        std::string name;
        
        // filename state "_D" or "_C" or -1
        int name_state;
        // detected state from @split_and_check_activity
        int detected_state;
        // index of the corresponding area from @findContours
        int max_area_index;
        // selected plane from @create_detection_planes
        int selected;
        
        // different metric to caracterize the detected goat shape for classification
        float hyperplane_distance;
        float area, solidity, aspect, perimeter;
        float extent, equivalent_diameter;
        float measurs[15];
        
    public:
        // the contours of the of the incomming plane
        std::vector<cv::Point> contours;
        // fitted ellipse of the contours in hough domain
        cv::RotatedRect minEllipse;
        // fitted ellipse inside the contours
        cv::RotatedRect maxInside;
        // the thresholded image of the incomming plane
        cv::Mat threshold;
        // the incomming plane from @create_detection_planes
        cv::Mat plane;
        // the original picture
        cv::Mat original;
};