#include "check.h"

#include "../tools/bank.h"
#include "../tools/argument.h"
#include "../tools/config.h"
#include "../detection/color.h"
#include "../detection/extract.h"
#include "../detection/shape.h"

Activity split_and_check_activity(Statistics &stats, const cv::Mat &original, int name_state, const char *filename, bool nogui, bool write, bool noout)
{
		std::vector<cv::Mat> planes;
		create_detection_planes(original, planes);
    
		std::vector<Activity> activities;
		
		for(int i = 0; i<planes.size(); ++i)
		{
				Activity a;
        
        a.selected = i;
        a.name_state = name_state;
        a.name = filename;
        
				int area_index = check_activity(planes[i], plane_name[i], a, nogui);
    
        if(write)
        {
            std::string filename = a.name;
            std::replace(filename.begin(), filename.end(), '/', '_');
            filename = std::string("area/") + plane_name[a.selected] + "_" + filename;
            cv::imwrite(filename, a.threshold);
        }
        
				if(area_index != -1)
						activities.push_back(a);
		}
		
		Activity mean(activities);
    
    mean.name = filename;
    mean.original = original;
    
    if(!noout)
        mean.print(stats.current_goat);
        
		stats.update(mean);
    
    return mean;
}

int check_activity(const cv::Mat &src, const char *name, Activity &a, bool nogui)
{
    cv::Mat threshold;
    
		thresholdAndMorph(src, threshold);
    
    cv::cvtColor(threshold, a.threshold, CV_GRAY2BGR);
    addWeighted(a.threshold, 1, a.threshold, 3, 0.0, a.threshold);
    a.plane = src;
        
    std::vector<std::vector<cv::Point>> contours;
    std::vector<cv::RotatedRect> minEllipse(contours.size());
    
    int max_area_index = findAreaAndFitEllipse(threshold, contours, minEllipse);
    
    if(max_area_index != -1)
    {
        a.width = std::min(minEllipse[max_area_index].size.width, minEllipse[max_area_index].size.height);
        a.height = std::max(minEllipse[max_area_index].size.width, minEllipse[max_area_index].size.height);
        a.x = minEllipse[max_area_index].center.x;
        a.y = minEllipse[max_area_index].center.y;
        
        a.solidity = getSurfaceSolidity(a.threshold, contours[max_area_index]);
        a.aspect = a.width / a.height;
        a.area = cv::contourArea(contours[max_area_index]);
        a.extent = a.width * a.height;
        a.equivalent_diameter = 1 / std::sqrt(4.f * a.area / M_PI);
				a.perimeter = cv::arcLength(contours[max_area_index], true);
        
        a.contours = contours[max_area_index];
        a.minEllipse = minEllipse[max_area_index];
        // a.maxInside = largestRectInNonConvexPoly(/*cv::Scalar::all(255) - */a.threshold);
        a.max_area_index = max_area_index;
				
				/* name according to:
				 *		Selection of Descriptors for Particle Shape Characterisation [p13]
				 * minimum and maxium feret diameter is found here :
				 *		https://blogs.mathworks.com/steve/2017/09/29/feret-diameter-introduction/
				 * we allready know the orientation of the "particle" so we took
				 * the width and height of the oriented "particle" : RotatedRect
				 */
				
				// mean of the feret diameter
				float D = (a.width+a.height) / 2.0;
				// minimum of the feret diameter
				float B = a.width;
				// area of the particle
				float A = a.area;
				// perimeter length of the particle
				float P = a.perimeter;
				// maximum feret diameter
				float L = a.height;
				// median of the feret diameter
				// we took the mean because we allready know the orientation
				float F = D;
        
        float pisq = std::sqrt(M_PI);
				
				a.measurs[0] = D/L;
				a.measurs[1] = B/D;
				a.measurs[2] = 2*std::sqrt(A) / (D*pisq);
				a.measurs[3] = M_PI*D/P;
				a.measurs[4] = B/L;
				a.measurs[5] = P/(M_PI*L);
				a.measurs[6] = B * pisq / (2*std::sqrt(A));
				a.measurs[7] = B * M_PI / P;
				a.measurs[8] = 2.0 * std::sqrt(M_PI * A) / P;
				a.measurs[9] = 1.0 / (F*P);
				
				a.measurs[10] = B/L;							 // AR  = aspect ratio
				a.measurs[11] = 4*M_PI*A / (P*P);	 // FF  = form factor
				a.measurs[12] = 4*A / (M_PI*L*L);	 // R   = roundness
				a.measurs[13] = M_PI * D / P;	     // Con = convexity
    }
    
    display_activity(name, src, threshold, a, nogui);
		
		return max_area_index;
}

void display_activity(const char *name, const cv::Mat &src, const cv::Mat &threshold, Activity &activity, bool nogui)
{
    if(nogui)
        return;
        
    cv::Mat drawing = src.clone();
    activity.threshold.copyTo(drawing);
    // src.copyTo(drawing, threshold);
    
    if(activity.max_area_index != -1)
    {
        cv::ellipse(drawing, activity.minEllipse, cv::Scalar(255, 64, 64), 1, 8);
        
        // getOrientation(activity.contours, drawing);
        
        cv::putText(
            drawing, std::to_string(activity.aspect).c_str(),
            cv::Point(3,8),
            cv::FONT_HERSHEY_COMPLEX_SMALL,
            0.5, cv::Scalar(255,0,255), 1, 0
        );
        
        cv::putText(
            drawing, std::to_string(activity.solidity).c_str(),
            cv::Point(3,drawing.size().height-3),
            cv::FONT_HERSHEY_COMPLEX_SMALL,
            0.5, cv::Scalar(255,0,255), 1, 0
        );
        
        auto extrem = getExtremPoint(activity.contours);
        
        cv::circle(drawing, extrem.first.first,   1, cv::Scalar(0,255,0));
        cv::circle(drawing, extrem.first.second,  1, cv::Scalar(0,255,0));
        cv::circle(drawing, extrem.second.first,  1, cv::Scalar(0,255,0));
        cv::circle(drawing, extrem.second.second, 1, cv::Scalar(0,255,0));
        
        // ellipse(drawing, activity.maxInside, Scalar(0, 0, 255), 1, 8);
    }
    
    cv::namedWindow(name, cv::WINDOW_NORMAL);
    cv::imshow(name, drawing);
}