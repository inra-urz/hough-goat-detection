#pragma once

#include "activity.h"

/**
 * statitics of global detected Goat
 * enable to update statistics from a detected Activity
 * enable to print a review of statics of detected goat in the end
 **/
struct Statistics
{
    public:
        /**
         * initialize internal field to the corresponding minimum and maximum value
         **/
        Statistics();
        
        /**
         * update statistical information based on a selected and detected Activity
         * from @split_and_check_activity
         **/
        void update(const Activity &detected);
        
        /**
         * print the final statistics information
         **/
        void print();
    public:
        //! the current goat to be detected
        int current_goat;
        //! count of correct matching goat when @Activity::is_correct return true
        int correct_match;
        //! count of the true possitive match : ie the correct eating state
        int true_positive;
        //! count of the true negative match : ie the correct sleeping state
        int true_negative;
        //! count of the false possitive match : ie the incorrect eating state
        int false_positive;
        //! count of the false negative match : ie the incorrect sleeping state
        int false_negative;
        //! count the valid detected goat (when they is not in error state)
        int valid_goat;
    public:
        //! minimal aspect ratio of a eating goat
        float aspect_eat;
        //! maximal aspect ratio of a sleeping goat
        float aspect_sleep;
        //! minimal solidity of a eating goat
        float solidity_sleep;
        //! maximal solidity of a eating goat
        float solidity_eat;
};
