#include "../tools/bank.h"
#include "../tools/argument.h"
#include "../tools/config.h"

#include "activity.h"
#include "check.h"

#include <iostream>
#include <iomanip>
#include <sstream>

Activity::Activity()
{
    x = y = -1;
    width = height = -1.0f;
    solidity = aspect = -1.0f;
    max_area_index = selected = -1;
    hyperplane_distance = area  = perimeter = -1;
    extent = equivalent_diameter = -1;
    
    std::memset(measurs, -1, 15*sizeof(float));
    name_state = detected_state = EDS_ERROR;
}

Activity::Activity(const cv::Mat &src, const char *plane_name)
{
    max_area_index = check_activity(src, plane_name, *this, false);
}

Activity::Activity(const std::vector<Activity> &activities)
{
    if(!activities.size())
    {
        width = height = -1.0f;
        solidity = aspect = -1.0f;
        max_area_index = selected = -1;
        hyperplane_distance = area  = perimeter = -1;
        extent = equivalent_diameter = -1;
        
        std::memset(measurs, -1, 15*sizeof(float));
        name_state = detected_state = EDS_ERROR;
        
        return;
    }
    
    *this = activities[0];
    
    for(int i = 0; i<activities.size(); ++i)
    {
				// any activity with low solidity is not good
        if(solidity > activities[i].solidity)
            *this = activities[i];
    }
        
    //detected_state = aspect > 0.545;
    //detected_state = aspect/solidity > 1.24;
    //detected_state = measurs[12] < solidity;
    //detected_state = measurs[1] > 0.71;
    //detected_state = measurs[7] > 0.61;
    //detected_state = measurs[12] > solidity;

    //! initial idea
    // in most of case the aspect ratio work
    //detected_state = aspect > 0.545;
    // we know that the measure[12] is exact for detecting sleeping state after the value 0.54
    //detected_state |= measurs[12] > 0.54;
    
    //detected_state = measurs[10] > 0.55;
    
    // hyperplane separator learned by perceptron
    // static const double model[] = {
        /*
        this is all learning plane with his accuracy on 125 goat (1k iteration in the linear regression)
     -> 90.4%  0.4690239241847382, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,      -0.2614281700147155
        80.8%  0.0, -0.0004199860036627606, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,  -0.0002725006031170811
        60.4%  0.0, 0.0, 0.04773147823071609, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,     -0.0032824965929732536
        80.8%  0.0, 0.0, 0.0, 0.5018870473480189, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,      -0.38731796677694513
     -> 92.0%  0.0, 0.0, 0.0, 0.0, 0.5001898954329935, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,      -0.35320074935409435
        84.8%  0.0, 0.0, 0.0, 0.0, 0.0, 0.4977584119443352, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,      -0.4528661604133741
        77.6%  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.48362232085083817, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,     -0.42851750950923106
     -> 90.4%  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4690239241847382, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,      -0.2614281700147155
        80.8%  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.660339940159782, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,      -0.682980357069797
        76.8%  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4976136333303648, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,      -0.39418940076725917
        89.6%  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.46772484384641316, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,     -0.2907179725703875
        80.0%  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4796915841014639, 0.0, 0.0, 0.0, 0.0, 0.0,      -0.381235704012713
        80.8%  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0006276926066079929, 0.0, 0.0, 0.0, 0.0,   -0.0006647102047609082
     -> 90.4%  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4690239241847382, 0.0, 0.0, 0.0,      -0.2614281700147155
        81.6%  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.44048105342343374, 0.0, 0.0,     -0.2805427956664299
        88.8%  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.4367092946997991, 0.0,      -0.2193749312602659
        77.6%  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.48362232085083817,     -0.42851750950923106
        */
        
        /*
        this is a learned hyperplane from the previous selected metric >= 90%
        accuracy    = 82.817787%  (6854/8276)
        sensitivity = 66.753242%  (1542/2310)
        specificity = 89.037880%  (5312/5966)
        precision   = 70.218582%  (1542/2196)
        4.227985085531441, 0.0, 0.0, 0.0, 2.453901914138717, 0.0, 0.0, 4.227985085531441, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -6.3236126343231405
        */
        
        /************************************************************************************************/
        
        /*
        all next plane have this result on the learning data
        accuracy    = 92.000000%  (115/125)
        sensitivity = 66.666672%  (16/24)
        specificity = 98.019806%  (99/101)
        precision   = 88.888893%  (16/18)
        */
        
        /*
        92% learned with initial vector detected_state = aspect > 0.545
        accuracy    = 82.769455%  (6850/8276)
        sensitivity = 68.181816%  (1575/2310)
        specificity = 88.417702%  (5275/5966)
        precision   = 69.505737%  (1575/2266)
        0.933877507127941, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.5041551115151993
        */
        
        /*
     -> 92% learned in 125, actualy, it's the best hyperplane parameter
        accuracy    = 82.817787%  (6854/8276)
        sensitivity = 62.640690%  (1447/2310)
        specificity = 90.630241%  (5407/5966)
        precision   = 72.133598%  (1447/2006)
        0.06696595572522847, 0.0, 0.0, 0.0, 0.03363893917701237, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.06696595572522847, 0.0, 0.07431067440843914, 0.0, -0.1337815784958322
        */
        
        /*
        92%: learned with initial vector in 125 : measurs[1] > 0.71
        accuracy    = 82.769455%  (6850/8276)
        sensitivity = 68.181816%  (1575/2310)
        specificity = 88.417702%  (5275/5966)
        precision   = 69.505737%  (1575/2266)
        0.0, 0.0, 0.0, 0.0, 0.9539317141872906, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.6688679578750198
        */
        
        /*
        92%% with mixed measurs[1] and aspect with initial vector in 125 : detected_state = aspect > 0.545
        accuracy    = 82.769455%  (6850/8276)
        sensitivity = 68.181816%  (1575/2310)
        specificity = 88.417702%  (5275/5966)
        precision   = 69.505737%  (1575/2266)
        0.902064279948571, 0.0, 0.0, 0.0, 0.8389102845709344, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -1.0751995855682124
        */
        
        /*
        91.19% - 87.38% : same but with higher iteration ~1M
        accuracy    = 82.709038%  (6845/8276)
        sensitivity = 66.666672%  (1540/2310)
        specificity = 88.920555%  (5305/5966)
        precision   = 69.968193%  (1540/2201)
        0.5658441723875965, 0.0, 0.0, -0.07711988335836288, 0.0, 0.04836258617648403, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -0.2903606984386047
        */
        
        /************************************************************************************************/
        
        /*
        accuracy    = 82.817787%  (6854/8276)
        sensitivity = 66.753242%  (1542/2310)
        specificity = 89.037880%  (5312/5966)
        precision   = 70.218582%  (1542/2196)
        9.933552541525934, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -5.398562499050152
        */
    
        /*
        accuracy    = 82.588211%  (6835/8276)
        sensitivity = 66.406921%  (1534/2310)
        specificity = 88.853500%  (5301/5966)
        precision   = 69.758980%  (1534/2199)
        9.790653928183849, 0.0, 0.0, 2.150213992574661, 0.0, -1.3536548846863796, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, -5.780227500571653
        */
        
        /*
        accuracy    = 82.757370%  (6849/8276)
        sensitivity = 67.186150%  (1552/2310)
        specificity = 88.786461%  (5297/5966)
        precision   = 69.878433%  (1552/2221)
        8.305479970743349, 0.0, -0.5052916379450398, 0.26461794529678967, 5.986029636643233, -3.6276770894634813, -6.340575674101685, 9.30537997074335, 0.0, 0.0, 0.0, -2.1015746592920856, 0.0017434654715164158, 9.30537997074335, 1.7823475834005793, 8.052677527880654, -6.340575674101685, -8.066045579413899
        */
        
        /*
        accuracy    = 75.676659%  (6263/8276)
        sensitivity = 75.930740%  (1754/2310)
        specificity = 75.578278%  (4509/5966)
        precision   = 54.624729%  (1754/3211)
         2.91289381e-01,  -1.14841829e-01,   6.03300763e-01,   3.62557119e-01,
        -2.06603483e-01,  -4.69271184e-01,  -1.77501913e+00,  -8.40028865e-01,
         1.76335725e+02,  -7.88668511e-01,  -1.28094140e+00,   2.45493276e+00,
        -4.10170675e-01,  -4.32712085e-01,  -8.83689557e-01,   3.79529329e-01,
        -1.59311954e+00,  -150.3232791310
        */
        /*
        9.26010841,  0.77027326, -0.37527804,  1.20660815,  6.52992286, -0.,         -0.,
       -0.       ,  -0.    ,     -0.,          0.     ,     0.   ,       0.,          0.,
        1.1776924 ,  7.41692302, -6.28492458, -9.29545105
        */
    //};
    
    //! now initialize the yperplane model on Config::hyperplane
    
    hyperplane_distance =
          Config::hyperplane[0]  * aspect
        + Config::hyperplane[1]  * solidity
        + Config::hyperplane[2]  * equivalent_diameter
        + Config::hyperplane[3]  * measurs[0]
        + Config::hyperplane[4]  * measurs[1]
        + Config::hyperplane[5]  * measurs[2]
        + Config::hyperplane[6]  * measurs[3]
        + Config::hyperplane[7]  * measurs[4]
        + Config::hyperplane[8]  * measurs[5]
        + Config::hyperplane[9]  * measurs[6]
        + Config::hyperplane[10] * measurs[7]
        + Config::hyperplane[11] * measurs[8]
        + Config::hyperplane[12] * measurs[9]
        + Config::hyperplane[13] * measurs[10]
        + Config::hyperplane[14] * measurs[11]
        + Config::hyperplane[15] * measurs[12]
        + Config::hyperplane[16] * measurs[13]
        + Config::hyperplane[17];
    
    detected_state = hyperplane_distance >= 0;
}

void Activity::print_header()
{
    if(ArgumentParsing::singleton->csv)
        Activity::print_csv_header();
    else
        Activity::print_shell_header();
}

void Activity::print_shell_header()
{
    std::stringstream ss;
    
    ss << std::setw(GoatBank::singleton->name_size()) << std::left
       << "filename"
       << "  --  "
       << std::fixed << std::setw(10)
       << "aspect"
       << "    state    "
       << " status "
       << std::fixed << std::setw(10)
       << "solidity"
       << "  --  "
       << "selected";

    std::cout << ss.str() << std::endl;
}

void Activity::print_csv_header()
{
    static const std::string header(
				"name;x;y;name_state;detected;status;"
				"aspect;solidity;equivalent_diameter;"
				"m0;m1;m2;m3;m4;"
				"m5;m6;m7;m8;m9;"
				"m10;m11;m12;m13"
		);
    std::cout << header << std::endl;
}

void Activity::print(int current_goat) const noexcept
{
    if(ArgumentParsing::singleton->csv)
        print_csv(current_goat);
    else
        print_shell(current_goat);
}

void Activity::print_shell(int current_goat) const noexcept
{
    static const char *state[] = {
        " >> eating   ",
        " >> sleeping ",
        " >> error    ",
        nullptr
    };
    
    static const char *status[] = {
        "        ",
        "  [ok]  ",
        "  <er>  ",
        nullptr
    };
    
    std::stringstream ss;
    
    unsigned int name_size = ArgumentParsing::singleton->detection ? 5 : GoatBank::singleton->name_size();
    
    ss << std::setw(name_size) << std::left
       << name
       << " -- "
       << std::fixed << std::setw(10) << std::right
       << hyperplane_distance
       << state[detected_state]
       << status[is_correct()]
       << std::fixed << std::setw(10) << std::right
       << solidity
       << " -- "
       << selected;
        
    std::cout << ss.str() << std::endl;
}

void Activity::print_csv(int current_goat) const noexcept
{
    const unsigned int start = name.find_last_of('/')+1;
    const unsigned int end = name.find_last_of('_');
    
    std::cout
        << name.substr(start, end-start) << ";"
        << x                      << ";"
        << y                      << ";"
        << name_state*0.1         << ";"
        << detected_state*0.2     << ";"
        << 0.8 + is_correct()*0.2 << ";"
        << aspect                 << ";"
        << solidity/2.0           << ";"
        << equivalent_diameter    << ";"
				
        << measurs[0] << ";"
        << measurs[1] << ";"
        << measurs[2] << ";"
        << measurs[3] << ";"
        << measurs[4] << ";"
        << measurs[5] << ";"
        << measurs[6] << ";"
        << measurs[7] << ";"
        << measurs[8] << ";"
        << measurs[9] << ";"
        << measurs[10] << ";"
        << measurs[11] << ";"
        << measurs[12] << ";"
        << measurs[13]
				
				<< std::endl;
}