#include "statistics.h"

#include <iostream>
#include <iomanip>

Statistics::Statistics()
{
    true_positive = 0;
    true_negative = 0;
    false_positive = 0;
    false_negative = 0;
        
    current_goat = 0;
    correct_match = 0;
    valid_goat = 0;
    aspect_eat = 1.0;
    aspect_sleep = 1.0;
}

void Statistics::update(const Activity &detected)
{
    unsigned status = detected.is_correct();
    
    if(detected.detected_state >= 0 && detected.detected_state < 2)
    {
        correct_match += status;
        
        true_positive += detected.name_state == 1 && detected.detected_state == 1;
        true_negative += detected.name_state == 0 && detected.detected_state == 0;
        false_positive += detected.name_state == 0 && detected.detected_state == 1;
        false_negative += detected.name_state == 1 && detected.detected_state == 0;
        
        if(detected.name_state == 1)
        {
            aspect_sleep = std::min(aspect_sleep, detected.aspect);
            solidity_sleep = std::min(solidity_sleep, detected.solidity);
        }
            
        if(detected.name_state == 0)
        {
            aspect_eat = std::max(aspect_eat, detected.aspect);
            solidity_eat = std::min(solidity_eat, detected.solidity);
        }
        
        valid_goat++;
    }
}

void Statistics::print()
{
    std::cout << "---------------------------------" << std::endl;
    
    float accuracy    = correct_match / float(          valid_goat          ) * 100.0f;
    float sensitivity = true_positive / float(true_positive + false_negative) * 100.f;
    float specificity = true_negative / float(true_negative + false_positive) * 100.f;
    float precision   = true_positive / float(true_positive + false_positive) * 100.f;
    
    std::cout << "accuracy    = " << std::fixed << accuracy    << "%  (" << correct_match << "/" <<             valid_goat           << ")" << std::endl;
    std::cout << "sensitivity = " << std::fixed << sensitivity << "%  (" << true_positive << "/" << (true_positive + false_negative) << ")" << std::endl;
    std::cout << "specificity = " << std::fixed << specificity << "%  (" << true_negative << "/" << (true_negative + false_positive) << ")" << std::endl;
    std::cout << "precision   = " << std::fixed << precision   << "%  (" << true_positive << "/" << (true_positive + false_positive) << ")" << std::endl;
    
    std::cout << "---------------------------------" << std::endl;
    
    std::cout << "aspect_eat     " << aspect_eat << std::endl;
    std::cout << "aspect_sleep   " << aspect_sleep << std::endl;
    std::cout << "solidity_eat   " << solidity_eat << std::endl;
    std::cout << "solidity_sleep " << solidity_sleep << std::endl;
}