#pragma once

#include "activity.h"
#include "statistics.h"

/**
 * create all detection plane from the original goat texture
 * detect goat and get his properties
 * select the goat with the best solidity factor
 * classify the goat depending of his aspect ratio
 * calculate the percent of good classification
 * print the selected activity properties
 * they also update statistics informations
 * @return the best Activity
 **/
Activity split_and_check_activity(Statistics &stats, const cv::Mat &original, int name_state, const char *filename, bool nogui = true, bool write = false, bool noout = false);

/**
 * detect the goat activity for the given plane texture
 * @see main_activity::split_and_check_activity
 * @return -1 on failure
 **/
int check_activity(const cv::Mat &src, const char *name, Activity &, bool nogui = true);

/**
 * used by check_activity, they display some detected properties for the given texture plane
 * @see check_activity
 **/
void display_activity(const char *name, const cv::Mat &src, const cv::Mat &threshold, Activity &activity, bool nogui = true);
    