# abstract

An animal's behavior can be useful indicator or their physological start. As resting,
walking and eating are the predominant daily activities of ruminant animals, monitoring
there behavior could provide valuable information for management decision and individual animal healt status.

Traditional animal monitoring have relied on labour intensive, human, observation of animals,
they also have hight economic cost for farmers or doing small study in low economic country.

Your technics aim to use image processing to localise animals and detecte his activities to
determine the economic loose from reduced performance due to bad heal,
or the propagation factor of virus, or could help to determine the eating factor
of a pasture by localising animals and theres activities,
or to help to determine the fessies drop from the animals during the day,
they also could help to count animals from statellite pictures.

Different algorithm, such robust ellipse fitting by hough transform,
canny edge detection, frequency analysis, color space manipulation,
particlues shape descriptor, machine learning, and many more
are used to developpe ours methode.

The proposed methode could detecte animals on the pasture from a image of the entire study land,
with great level of sensitivity, specificity accuracy, and precision, 82.709%, 66.6667%, 88.9206% and 69.9682% respectively tested over 8710 samples.
The result show that it is possible to use machine vision and machine learning techniques,
in order to automatically detect animals and there activity under ground condition.

The main properties of ours algorithms is to be easely adapted for other purpose, and it's easy to add new color space plane or shape metric
with automatic detection of the best criteria from there new factors.

Further research is required to increase the accuracy of our algorithm by adding new color-space and shape characterisation metrics.

# 1. Introduction

[------------------------]
      long description
[------------------------]

# 2. Material and methodes

this study was conducted at the INRA of the Guadeloupe (coord gps)
the study use a groupe of 30 goat over two campagne 04/17 and 05/17
in two pasture with four consicutive days for each one and 19 shot per day.

## 2.1. animals

all position have been manualy detected, and also there activity.
There result have been studed to create ours algorithm and used validating our approche.
a sub set of 125 goat have been used for the learning stage by linear hyperplane regression in 15 dimension

## 2.2. camera

A [-------] drone equiped with [------] camera,
the resolution is about of 4000x3000 with [12] Mega pixel without len distortion,
and the goat have a very small resolution,
the young entities have a size between 20x20 and 30x10,
and the old entities have a size between 30x30 and 45x15,
meaning that the activity recognition is hard

## 2.3. drones

The location of the drone is approximetely at the center of the land with an approximated distance from the land of [--] feet,
all part of the shot that is not in the cadastral of the pasture is removed,
this allow to remove unexecpted result, like animals of other pasture study,
detecting farmers, vehicules, or other vegetal noise for performances.

# 3. Results and dicussion

[------------------------]
      long description
[------------------------]

# 4. Conclusion

In this study, automatic detection of animals on the field based on ellipse feating and color space manipulation was repported.
A color space manipulation have be used to remove the weed of the pictures, this allow to mark feature as blobs in the color space.
Once the background have been removed, a threshold and morphological operator is applied to each color plane to enable to use edge detection algorithm such as canny.
This allow to use robust hough ellipse detection to detect features on the land image.
Thoses features are analysed to determine if the sub-texture is a goat or not using a bank of goat (texture and shape properties).

The activity recognition part of this study have also been repported.
The sub-texture of the field corresponding to the goat are use as an input texture, the same methode are used with more accurate parameters.
Once the background, the threshold and morphological operator are applied, the form are characterised with particules descriptor métrics.
This metric determine an hyperplane class sperator, and there parameters have been learning with regression alogorithms such as perceptron.
The learned hyperplane are use to classify the goat on two class, the sleeping one and the eating one.

The performance of the algorithm showed a great level of accuracy,
so the method could help the study of large campagne such as drone or satelitte animals counting and there behavior.

The main properties of ours algorithms is to be easely adapted for other purpose, and it's easy to add new color space plane or shape metric
with automatic detection of the best criteria from there new factors.

Future developpement are in the mind, the next step is to increase the accuracy of the methode by adding new color-space manipulation
and new shape characterisation metrics.

# 5. Further research

Our algorithm is stable again the selection of color space with the learned bias of the solidity factor,
they are also stable again the metric selection with the learning hyperplane parameters.

The first main challenges is to found a better metric to characterise the shape,
this would increase the accuracy of sleeping or eating activity detection,
especialy when the goat is sleeping on is edge or turning his head to watch backward.

The second challenge is to found new color space manipulation to detect edge in the texture,
like wood or wall that interfer for the sleeping activity detection,
this add region to the detected goat part that slitty increase the aspect ratio.

We can expect 93% of good result in place of 88%
with 92% for sensitivity in place of 82.7% (eating)
and with 85% for specificity in place of 66.7% (sleeping)

This current study aim to classify two sheep behaviours,
the further research could detect a new interesting state named "walking",
this can be done by anaylising the shadow of the goat by finding the inverse transform projection plane,
an other way is to predict the new position based on the orientation of the shape between two laps to calculate the speed of the goat,
the small one are eating or sleeping, and the others are walking.

the further research could also detect a new interesting informations about goat to help to inditification,
better shape caracteristation would detect the weight, and the age of the goat.

# 6. Acknoledgments

This study was funded by the Inra research center in Guadeloupe.
We also like to thank [-------] for their assistance

# 7. References

{Robust Skin Colour Detection ISSN:2278-0181}
{Removal of weeds using Image Processing ISSN:2319-7900}
{A Computational Approach to Edge Detection ISSN:0162-8828}
{Robust ellipse detection PMID:21478953}
{Texture classification using spectral histograms ISSN:1057-7149}
{Selection of Descriptors for Particle Shape Characterization}