# 0 gray scale
>   from color-space-manipulation

# 1 thresholding
>   apply threshold with V > 240
>   tested on goat & map

# 2 morphological operation
>   dilate to reconnect head and corps or bad detected
>   erode remove the contour added by dilate
>   morphological operation : MORPH_ELLIPSE
>   size = 3 (best one tested)

# 3 edge detection
>   canny + hough + ellipse fitting

# 4 properties check
>   aspect ratio > 0.55 ? eating : sleeping
>   learned by probability (previously checked)

# 5 verification with the filename
>   set/Black/*_D.png = eating
>   set/Black/*_C.png = sleeping