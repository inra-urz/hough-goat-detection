# 1 sharpen
>    augmente les coutours et donc la detection

# 2 blur
>    suprime le bruit induit par sharpen

# 3 remove yellow
>   suprimme l'herbe griller

# 4 saturate
>   augmente la distinction herbe / autre
>   les chèvres resorte bleu ou rouge

# 5 blur
>   propage les regions R/B dans l'herbe
>   ce qui permet de reconecter le coup

# 6 bilateral
>   unification des couleurs ~similaire

# 7 blur
>   propagation de la couleur R/B en violet
>   améliore la detection d'activiter de 5%
> + perspectif d'amélioration

# 8 remove_green_major
>   G+5 > max(R,B) || G > 110
>   suprimme l'herbe (vérifier sur l'histogramme)

# 9 gray scale