# perspective d'amélioration
## utilise la meilleur couleur
``
si sum(R)/sum(B) > alpha
    B inter R -> violet -> fitEllipse
sinon si < alpha
    max(R, B) -> fitEllipse
sinon
    coeficient de soliditer
    aire(surface) / aire(convexe_hull)
``

> 14_D, 58_D, 88_C, 57_C, 80_C, 81_C

## utiliser un autre espace de couleur
``
  si sum(R)+sum(B) < beta -> mauvaise detection
      utiliser HSV.v ou un autre espace
``

>  83_D, 90_C

# resultat attendu

> 96%