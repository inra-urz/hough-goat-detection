#!/usr/bin/python3

from sklearn.model_selection import GridSearchCV, train_test_split, cross_val_score
from sklearn.metrics import classification_report, confusion_matrix 
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import chi2

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.neighbors import KNeighborsClassifier

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
import graphviz 
import itertools

# --------------------------------------

def plot_confusion_matrix(cm, classes,  normalize=False, title='Confusion matrix', cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

# --------------------------------------

df = pd.read_csv('result-2.csv', sep=';')
np.set_printoptions(precision=2)

class_name = ['debout','coucher']
X = df.drop('name_state', axis=1)
X = X.drop('name', axis=1)
X = X.drop('detected', axis=1)
X = X.drop('status', axis=1)
y = (df['name_state']*10)

X = SelectKBest(chi2, k=3).fit_transform(X, y)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.90)  

# --------------------------------------

classifier = [
    ('DecisionTree',     DecisionTreeClassifier(max_depth=1, min_samples_split=5, random_state=0)),
    ('RandomForest',     RandomForestClassifier(n_estimators=10, max_depth=None, min_samples_split=2, random_state=0)),
    ('ExtraTrees',       ExtraTreesClassifier(n_estimators=10, max_depth=None, min_samples_split=2, random_state=0)),
    ('AdaBoost',         AdaBoostClassifier(n_estimators=100)),
    ('GradientBoosting', GradientBoostingClassifier(n_estimators=100, learning_rate=1.0, max_depth=1, random_state=0)),
    #('KNeighbors',       KNeighborsClassifier(n_neighbors=1))
]

for label, clf in classifier:

    print('-------------------------- [%s] --------------------------' % label)
    print('')
    
    clf.fit(X_train, y_train)
    y_pred = clf.predict(X_test)

    plt.figure()
    M = confusion_matrix(y_test, y_pred)
    plot_confusion_matrix(M, class_name, normalize=True)
    plt.savefig('Classifier'+ label + '.jpg')
    
    print(classification_report(y_test, y_pred))
    
    print('')
pass

print('-------------------------- [%s] --------------------------' % 'Voting')
    
clf = VotingClassifier(estimators=classifier, voting='soft')
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

plt.figure()
M = confusion_matrix(y_test, y_pred)
plot_confusion_matrix(M, class_name, normalize=True)
plt.savefig('Classifier'+ 'Voting' + '.jpg')

print(classification_report(y_test, y_pred))

# --------------------------------------
