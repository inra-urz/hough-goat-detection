#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

Mat src; Mat src_gray;
int thresh = 100;
int max_thresh = 255;
RNG rng(12345);

/// Function header
void thresh_callback(int, void* );

/** @function main */
int main( int argc, char** argv )
{
    /// Load source image and convert it to gray
    src = imread( argv[1], 1 );

    /// Convert image to gray and blur it
    //cvtColor( src, src, CV_RGB2HSV );
    cvtColor( src, src_gray, CV_BGR2GRAY );
    //blur( src_gray, src_gray, Size(5,5) );
    
    /*
    unsigned int lowThreshold = 10;
    unsigned int ratio = 100;
    unsigned int kernel = 5;
    
    Canny( src_gray, src_gray, lowThreshold, lowThreshold*ratio, kernel );
    */

    /// Create Window
    char* source_window = "Source";
    namedWindow( source_window, WINDOW_NORMAL );
    imshow( source_window, src_gray );

    createTrackbar( " Threshold:", "Source", &thresh, max_thresh, thresh_callback );
    thresh_callback( 0, 0 );

    while(true)
    waitKey(0);
    
    return(0);
}

/** @function thresh_callback */
void thresh_callback(int, void* )
{
    Mat threshold_output;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    /// Detect edges using Threshold
    threshold( src_gray, threshold_output, thresh, 255, THRESH_BINARY );
    /// Find contours
    findContours( threshold_output, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );

    /// Find the rotated rectangles and ellipses for each contour
    vector<RotatedRect> minRect( contours.size() );
    vector<RotatedRect> minEllipse( contours.size() );
    vector<bool>        enabled( contours.size() );

    for( int i = 0; i < contours.size(); i++ )
    {
        minRect[i] = minAreaRect( Mat(contours[i]) );
        if( contours[i].size() > 10 && contours[i].size() < 200 )
            minEllipse[i] = fitEllipse( Mat(contours[i]) );
            enabled[i] = true;
    }

    /// Draw contours + rotated rects + ellipses
    Mat drawing = Mat::zeros( threshold_output.size(), CV_8UC3 );
    for( int i = 0; i< contours.size(); i++ )
    {
        float size = minEllipse[i].size.width * minEllipse[i].size.height;
        float aspect = minEllipse[i].size.width / float(minEllipse[i].size.height);
        
        if(size < 10 || !enabled[i] || size > 3000)
            continue;
            
        if(aspect < 0.3 || aspect > 0.7)
            continue;

        Scalar color = Scalar( rng.uniform(100, 255), rng.uniform(100,255), rng.uniform(100,255) );
        // contour
        drawContours( drawing, contours, i, color, 1, 8, vector<Vec4i>(), 0, Point() );
        // ellipse
        ellipse( drawing, minEllipse[i], color, 2, 8 );
        // rotated rectangle
        Point2f rect_points[4]; minRect[i].points( rect_points );
        
        for( int j = 0; j < 4; j++ )
            line( drawing, rect_points[j], rect_points[(j+1)%4], color, 1, 8 );
    }
   
    Mat dst;
    double alpha = 0.5;
    double beta = ( 1.0 - alpha );
    addWeighted( src, alpha, drawing, beta, 0.0, dst);

    /// Show in a window
    namedWindow( "Contours", WINDOW_NORMAL );
    imshow( "Contours", dst );
}