%%% Initial pixel value
I = double(imread('I.jpg'));
J = find(sum(I,3) < (3*255));
R = squeeze(I(:,:,1));
G = squeeze(I(:,:,2));
B = squeeze(I(:,:,3));

lab_he = double(rgb2lab(imread('I.jpg')));
L = squeeze(lab_he(:,:,1));
A = squeeze(lab_he(:,:,3));
b = squeeze(lab_he(:,:,3));

lab_he = double(rgb2lin(imread('I.jpg')));
Lin1 = squeeze(lab_he(:,:,1));
Lin2 = squeeze(lab_he(:,:,3));
Lin3 = squeeze(lab_he(:,:,3));

Varigreen = (squeeze(I(:,:,2)) - squeeze(I(:,:,1)))./...
    (squeeze(I(:,:,2)) + squeeze(I(:,:,1)) - squeeze(I(:,:,3)));


M = G;
Diff_G = (M - mean(mean(M)).*ones)./(var(var(M)));

M = Varigreen;
Diff_V = (M - mean(mean(M)).*ones)./(var(var(M)));


%%% Goats
I = double(imread('I_OnlyGoats.JPG'));
Idx = find(sum(I,3) ~= 0);

Goats = [Goats ; L(Idx) , A(Idx) , b(Idx) , R(Idx) , G(Idx) , B(Idx) , ...
    Lin1(Idx) , Lin2(Idx) , Lin3(Idx) , Varigreen(Idx) , Diff_G(Idx) , Diff_V(Idx)]; 

%%% Wood
I = double(imread('I_Bois.JPG'));
Idx = find(sum(I,3) ~= 0);

Wood = [L(Idx) , A(Idx) , b(Idx) , R(Idx) , G(Idx) , B(Idx) , ...
    Lin1(Idx) , Lin2(Idx) , Lin3(Idx) , Varigreen(Idx) , Diff_G(Idx) , Diff_V(Idx)]; 
%%% Wood 2
I = double(imread('I_Bois2.JPG'));
Idx = find(sum(I,3) ~= 0);

Wood2 = [L(Idx) , A(Idx) , b(Idx) , R(Idx) , G(Idx) , B(Idx) , ...
    Lin1(Idx) , Lin2(Idx) , Lin3(Idx) , Varigreen(Idx) , Diff_G(Idx) , Diff_V(Idx)]; 

%%% Dark Weed
I = double(imread('I_HerbeGrille.JPG'));
Idx = find(sum(I,3) ~= 0);

DWeed = [L(Idx) , A(Idx) , b(Idx) ,  R(Idx) , G(Idx) , B(Idx) , ...
    Lin1(Idx) , Lin2(Idx) , Lin3(Idx) , Varigreen(Idx) , Diff_G(Idx) , Diff_V(Idx)]; 

%%% Average Weed
I = double(imread('I_HerbeMoyenne.JPG'));
Idx = find(sum(I,3) ~= 0);

AWeed = [L(Idx) , A(Idx) , b(Idx) , R(Idx) , G(Idx) , B(Idx) , ...
    Lin1(Idx) , Lin2(Idx) , Lin3(Idx) , Varigreen(Idx) , Diff_G(Idx) , Diff_V(Idx)]; 

%%% Green Weed
I = double(imread('I_HerbeVerte.JPG'));
Idx = find(sum(I,3) ~= 0);

GWeed = [L(Idx) , A(Idx) , b(Idx) , R(Idx) , G(Idx) , B(Idx) , ...
    Lin1(Idx) , Lin2(Idx) , Lin3(Idx) , Varigreen(Idx) , Diff_G(Idx) , Diff_V(Idx)]; 

figure('name','L')
subplot(2,3,1);hold on;title('Goats');hist(Goats(:,1),0:100);
subplot(2,3,2);hold on;title('Wood');hist(Wood(:,1),0:100);
subplot(2,3,3);hold on;title('Wood2');hist(Wood2(:,1),0:100);
subplot(2,3,4);hold on;title('DWeed');hist(DWeed(:,1),0:100);
subplot(2,3,5);hold on;title('AWeed');hist(AWeed(:,1),0:100);
subplot(2,3,6);hold on;title('GWeed');hist(GWeed(:,1),0:100);

figure('name','A')
subplot(2,3,1);hold on;title('Goats');hist(Goats(:,2),-300:299)
subplot(2,3,2);hold on;title('Wood');hist(Wood(:,2),-300:299)
subplot(2,3,3);hold on;title('Wood2');hist(Wood2(:,2),-300:299)
subplot(2,3,4);hold on;title('DWeed');hist(DWeed(:,2),-300:299)
subplot(2,3,5);hold on;title('AWeed');hist(AWeed(:,2),-300:299)
subplot(2,3,6);hold on;title('GWeed');hist(GWeed(:,2),-300:299)

figure('name','b')
i = 3;
subplot(2,3,1);hold on;title('Goats');hist(Goats(:,i),-300:299)
subplot(2,3,2);hold on;title('Wood');hist(Wood(:,i),-300:299)
subplot(2,3,3);hold on;title('Wood2');hist(Wood2(:,i),-300:299)
subplot(2,3,4);hold on;title('DWeed');hist(DWeed(:,i),-300:299)
subplot(2,3,5);hold on;title('AWeed');hist(AWeed(:,i),-300:299)
subplot(2,3,6);hold on;title('GWeed');hist(GWeed(:,i),-300:299)

figure('name','R')
i = 4;
subplot(2,3,1);hold on;title('Goats');hist(Goats(:,i),0:255)
subplot(2,3,2);hold on;title('Wood');hist(Wood(:,i),0:255)
subplot(2,3,3);hold on;title('Wood2');hist(Wood2(:,i),0:255)
subplot(2,3,4);hold on;title('DWeed');hist(DWeed(:,i),0:255)
subplot(2,3,5);hold on;title('AWeed');hist(AWeed(:,i),0:255)
subplot(2,3,6);hold on;title('GWeed');hist(GWeed(:,i),0:255)

figure('name','G')
i = 5;
subplot(2,3,1);hold on;title('Goats');hist(Goats(:,i),0:255)
subplot(2,3,2);hold on;title('Wood');hist(Wood(:,i),0:255)
subplot(2,3,3);hold on;title('Wood2');hist(Wood2(:,i),0:255)
subplot(2,3,4);hold on;title('DWeed');hist(DWeed(:,i),0:255)
subplot(2,3,5);hold on;title('AWeed');hist(AWeed(:,i),0:255)
subplot(2,3,6);hold on;title('GWeed');hist(GWeed(:,i),0:255)

figure('name','B')
i = 6;
subplot(2,3,1);hold on;title('Goats');hist(Goats(:,i),0:255)
subplot(2,3,2);hold on;title('Wood');hist(Wood(:,i),0:255)
subplot(2,3,3);hold on;title('Wood2');hist(Wood2(:,i),0:255)
subplot(2,3,4);hold on;title('DWeed');hist(DWeed(:,i),0:255)
subplot(2,3,5);hold on;title('AWeed');hist(AWeed(:,i),0:255)
subplot(2,3,6);hold on;title('GWeed');hist(GWeed(:,i),0:255)

figure('name','Diff_G')
i = 11;
subplot(2,3,1);hold on;title('Goats');hist(Goats(:,i),linspace(min(min(Diff_G)),max(max(Diff_G)),200))
subplot(2,3,2);hold on;title('Wood');hist(Wood(:,i),linspace(min(min(Diff_G)),max(max(Diff_G)),200))
subplot(2,3,3);hold on;title('Wood2');hist(Wood2(:,i),linspace(min(min(Diff_G)),max(max(Diff_G)),200))
subplot(2,3,4);hold on;title('DWeed');hist(DWeed(:,i),linspace(min(min(Diff_G)),max(max(Diff_G)),200))
subplot(2,3,5);hold on;title('AWeed');hist(AWeed(:,i),linspace(min(min(Diff_G)),max(max(Diff_G)),200))
subplot(2,3,6);hold on;title('GWeed');hist(GWeed(:,i),linspace(min(min(Diff_G)),max(max(Diff_G)),200))

figure('name','Diff_V')
i = 12;
subplot(2,3,1);hold on;title('Goats');hist(Goats(:,i),linspace(min(min(Diff_V)),max(max(Diff_V)),200))
subplot(2,3,2);hold on;title('Wood');hist(Wood(:,i),linspace(min(min(Diff_V)),max(max(Diff_V)),200))
subplot(2,3,3);hold on;title('Wood2');hist(Wood2(:,i),linspace(min(min(Diff_V)),max(max(Diff_V)),200))
subplot(2,3,4);hold on;title('DWeed');hist(DWeed(:,i),linspace(min(min(Diff_V)),max(max(Diff_V)),200))
subplot(2,3,5);hold on;title('AWeed');hist(AWeed(:,i),linspace(min(min(Diff_V)),max(max(Diff_V)),200))
subplot(2,3,6);hold on;title('GWeed');hist(GWeed(:,i),linspace(min(min(Diff_V)),max(max(Diff_V)),200))


Data = cell(6,1);

Data{1,1} = Goats;
Data{2,1} = Wood;
Data{3,1} = Wood2;
Data{4,1} = DWeed;
Data{5,1} = AWeed;
Data{6,1} = GWeed;


Proba_L = zeros(101,6);
Proba_a = zeros(600,6);
Proba_b = zeros(600,6);
Proba_R = zeros(256,6);
Proba_G = zeros(256,6);
Proba_B = zeros(256,6);
Proba_L1 = zeros(256,6);
Proba_L2 = zeros(256,6);
Proba_L3 = zeros(256,6);
Proba_V = zeros(201,6);
Proba_DiffV = zeros(200,6);
Proba_DiffG = zeros(200,6);

vV = linspace(min(min(Diff_V)),max(max(Diff_V)),200);
vG = linspace(min(min(Diff_G)),max(max(Diff_G)),200);

for i = 1:6
    
    D = Data{i,1};
    Proba_L(:,i) = hist(D(:,1),0:100)./numel(D(:,1));
    Proba_a(:,i) = hist(D(:,2),-300:299)./numel(D(:,1));
    Proba_b(:,i) = hist(D(:,3),-300:299)./numel(D(:,1));
    Proba_R(:,i) = hist(D(:,4),0:255)./numel(D(:,1));
    Proba_G(:,i) = hist(D(:,5),0:255)./numel(D(:,1));
    Proba_B(:,i) = hist(D(:,6),0:255)./numel(D(:,1));
    Proba_L1(:,i) = hist(D(:,7),0:255)./numel(D(:,1));
    Proba_L2(:,i) = hist(D(:,8),0:255)./numel(D(:,1));
    Proba_L3(:,i) = hist(D(:,9),0:255)./numel(D(:,1));
    Proba_V(:,i) = hist(D(:,10),-1:.01:1)./numel(D(:,1));
    Proba_DiffV(:,i) = hist(D(:,11),vV)./numel(D(:,1));
    Proba_DiffG(:,i) = hist(D(:,12),vG)./numel(D(:,1));
end

    
save('Proba.mat','Proba_L','Proba_a','Proba_b','Proba_R','Proba_G','Proba_B','Proba_L1',...        
    'Proba_L2','Proba_L3','Proba_V','Proba_DiffV','Proba_DiffG','vV','vG')




