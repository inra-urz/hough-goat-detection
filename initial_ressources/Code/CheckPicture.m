function CheckPicture(Data,Day,Groupe)

filename = strvcat('100417','110417','120417','130417','150517','160517','170517','180517','250717','260717','270717');

chemin = ['C:\Users\mtbonneau\Documents\Stage\2017\DD_Medhy\' filename(Day,:) '\g' int2str(Groupe) '\RASTER\'];
Us = pwd;

cd(chemin)

I = find((Data(:,1) == Day)&(Data(:,2) == Groupe));
U = unique(Data(I,3));

for i = 1:length(U)
    
    J = intersect(I,find(Data(:,3) == U(i)));
    X = Data(J,4);
    Y = Data(J,5);
    
    if X(1) ~= -1
        [A , R] = geotiffread([int2str(i) '.tif']);
        
        if R.XWorldLimits(1) > 0
            
            cd(Us)
            [x1 , y1 , UN1 , UN2] = wgs2utm(Y,X,20,'N');
            cd(chemin)
            X = x1;
            Y = y1;
        end
        
        hour = floor(U(i)/60);
        minute = U(i) - hour*60;
        figure('Name',[num2str(hour) 'H' num2str(minute)],'units','normalized','outerposition',[0 0 1 1])
        mapshow(A,R)
        hold on
        mapshow(X,Y,'DisplayType','multipoint')
        pause
        close all
    end
end
cd(Us)




