function StaticFrequency(Groupe,Data,Days)


load RectFit.mat
U = unique(Data.Idx);

figure('Name',['Groupe ' int2str(Groupe)],'units','normalized','outerposition',[0 0 1 1])
hold on
axis image
[A,R] = geotiffread('CarteBase.tif');
mapshow(A,R)
I = find(Data.Groupe == Groupe);
Time = unique(Data.Time(I));
H = zeros(1,length(U));
H2 = 0;

for Day = Days(1):Days(2)
    
    ind = intersect(I,find(Data.Day == Day));
    Time = unique(Data.Time(ind));
    for i = 1:numel(Time)
        
        F = find(Data.Time(ind) == Time(i));
        h = hist(Data.Idx(I(F)),U);
        h(h>0) = ones;
        H = H + h;
        
    end
    H2 = H2 + numel(Time);
end
H = H./H2;

J = U(H>0)';
c = H(H>0);
x = [Data.Cx1(J) ; Data.Cx2(J) ; Data.Cx3(J) ; Data.Cx4(J)];
y = [Data.Cy1(J) ; Data.Cy2(J) ; Data.Cy3(J) ; Data.Cy4(J)];
patch(x,y,c,'EdgeColor','w')
C = rot90(rot90(colormap(gray(50))));
colormap(C)
caxis([0 0.15])
colorbar

if Groupe == 1
    plot(Data.z10(:,1),Data.z10(:,2))
    plot(Data.z11(:,1),Data.z11(:,2))
    plot(Data.z12(:,1),Data.z12(:,2))
    plot(Data.z13(:,1),Data.z13(:,2))
    plot(Data.z14(:,1),Data.z14(:,2))
    plot(Data.z15(:,1),Data.z15(:,2))
    plot(Data.z16(:,1),Data.z16(:,2))
    plot(Data.z17(:,1),Data.z17(:,2))
    plot(Data.z18(:,1),Data.z18(:,2))
    plot(Data.z19(:,1),Data.z19(:,2))
end
if Groupe ==2
    plot(Data.z21(:,1),Data.z21(:,2))
    plot(Data.z22(:,1),Data.z22(:,2))
    plot(Data.z23(:,1),Data.z23(:,2))
    plot(Data.z24(:,1),Data.z24(:,2))
    plot(Data.z25(:,1),Data.z25(:,2))
    plot(Data.z26(:,1),Data.z26(:,2))
    plot(Data.z27(:,1),Data.z27(:,2))
end
axis off
set(gca,'FontSize',16,'FontName','TimeNewsRoman')
drawnow
frame = getframe(gcf);
im = frame2im(frame);
I2 = imcrop(im,rect);

imwrite(I2,['Groupe_' int2str(Groupe) '_Days' int2str(Days(1)) '_' int2str(Days(2)) '.png'])