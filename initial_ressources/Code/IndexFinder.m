function I = IndexFinder(Data,D)
% Input : - D is the discrimination parameter. It is 1*3 and contains the group number,
% the day number and the time. If one element is negative, then this parameter is not accounted
% Output : - I is the correspondig index in Data that correspond to request in D.

r = 1;
if D(1) >0
    I_Groupe = find(Data.Groupe == D(1));
    r = 2*r;
end

if D(2) > 0
    I_Day = find(Data.Day == D(2));
    r = 3*r;
end

if D(3) > 0
    I_Time = find(Data.Time == D(3));
    r = 5*r;
end

switch r
    
    case 1
        I = [];
    case 2
        I = I_Groupe;
    case 3 
        I = I_Day;
    case 5
        I = I_Time;
    case 6
        I = intersect(I_Groupe,I_Day);
    case 10
        I = intersect(I_Groupe,I_Time);
    case 15
        I = intersect(I_Day,I_Time);
    case 30
        I = intersect(intersect(I_Day,I_Time),I_Groupe);
    otherwise
        I = [];
end
        
        
    