function ShowHistFreq(Data,Groupe)

U = unique(Data.Idx);




Week = 1;

switch Week
    case 1
        Days = [1 4];
    case 2
        Days = [5 8];
    case 3 
        Days = [9 11];
    otherwise
end


k = 1;
Sum = zeros(1,Days(2) - Days(1) + 1);

for Day = Days(1):Days(2)
    
    I = find((Data.Day == Day)&(Data.Groupe == Groupe));
    U = unique(Data.Idx(I));
    H = hist(Data.Idx(I),U);

    NbPics = sum(Data.NumPics((Data.NumPics(:,1) == Day)&...
        (Data.NumPics(:,2) == Groupe),3));

    H = H./NbPics;
    Sum(k) = sum(sum(H));
    k = k + 1;
end



figure 
hold on
grid on
plot(1:4,Sum,'-+r','LineWidth',3)



Week = 2;

switch Week
    case 1
        Days = [1 4];
    case 2
        Days = [5 8];
    case 3 
        Days = [9 11];
    otherwise
end

k = 1;
Sum = zeros(1,Days(2) - Days(1) + 1);

for Day = Days(1):Days(2)
    
    I = find((Data.Day == Day)&(Data.Groupe == Groupe));
    U = unique(Data.Idx(I));
    H = hist(Data.Idx(I),U);

    NbPics = sum(Data.NumPics((Data.NumPics(:,1) == Day)&...
        (Data.NumPics(:,2) == Groupe),3));

    H = H./NbPics;
    Sum(k) = sum(sum(H));
    k = k + 1;
end

plot(1:4,Sum,'-+k','LineWidth',3)



Week = 3;

switch Week
    case 1
        Days = [1 4];
    case 2
        Days = [5 8];
    case 3 
        Days = [9 11];
    otherwise
end

k = 1;
Sum = zeros(1,Days(2) - Days(1) + 1);

for Day = Days(1):Days(2)
    
    I = find((Data.Day == Day)&(Data.Groupe == Groupe));
    U = unique(Data.Idx(I));
    H = hist(Data.Idx(I),U);

    NbPics = sum(Data.NumPics((Data.NumPics(:,1) == Day)&...
        (Data.NumPics(:,2) == Groupe),3));

    H = H./NbPics;
    Sum(k) = sum(sum(H));
    k = k + 1;
end

plot(2:4,Sum,'-+b','LineWidth',3)

legend('Semaine 1','Semaine 2','Semaine 3')
xlabel('Jours')
ylabel('Fr�quences cumul�es')
set(gca,'FontName','TimeNewsRoman','FontSize',16)