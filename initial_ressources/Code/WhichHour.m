function res = WhichHour(h)

h1 = [8:17]*60;
res = find(h >= h1,1,'last');