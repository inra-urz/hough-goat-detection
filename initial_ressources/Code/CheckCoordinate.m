Name1 = '1805171110g2_georef.tif';
Name2 = 'shape11d4g2.shp';
chemin = 'C:\Users\mtbonneau\Documents\Stage\2017\DD_Medhy\180517\g2';


Us = pwd;

cd(chemin)

cd RASTER
[A, R] = geotiffread(Name1);
cd ..
cd SHAPEFILE
format long
S = shaperead(Name2);
N = zeros(length(S),2);
N(:,1) = transpose(table2array(table(S(:).X)));
N(:,2) = transpose(table2array(table(S(:).Y)));

cd(Us)

if (R.XWorldLimits(1) > 0) || (N(1,1) < 0)

    [x1 , y1 , UN1 , UN2] = wgs2utm(N(:,2),N(:,1),20,'N');
    N = [x1 , y1];
end
[x , y] = R.worldToDiscrete(N(:,1),N(:,2));
A2 = A;
for i = 1:length(x)
    d = 5;
    A2((x(i) - d):(x(i) + d),(y(i) - d):(y(i) + d),1) = 255;
    A2((x(i) - d):(x(i) + d),(y(i) - d):(y(i) + d),2:3) = 0;
end

figure;imagesc(A2);axis image



Pd = [];
for i = 1:length(x)
%     M_V = Varigreen((x(i) - 50):(x(i) + 50),(y(i) - 50):(y(i) + 50));
%     M_G = G((x(i) - 50):(x(i) + 50),(y(i) - 50):(y(i) + 50));
d = 50;
    J = A((x(i) - d):(x(i) + d),(y(i) - d):(y(i) + d),:);
    figure
    subplot(1,2,1);imagesc(J);axis off
%     O = sort(M_V(:));
    Res = Detect(J);
%     Res = kmeans(M_V(:),2,'Replicates',5,'Start',[O(1:5) , O((end-4):end)]');
%     M = squeeze(double(J(:,:,1))) + 2.*squeeze(double(J(:,:,2))) + squeeze(double(J(:,:,3)));
%     Res = kmeans(M(:),2);
%     subplot(1,2,2);imagesc(reshape(Res,size(J)))
    subplot(1,2,2);imagesc(Res)
    axis off
    %%% Find color
%     C = reshape(Res,size(M_I));
    C = Res;
    C = C(39:61,39:61);
    Pd = strvcat(Pd,Detect_Color(J(39:61,39:61,:),find(C == 0)));
end
Pd

