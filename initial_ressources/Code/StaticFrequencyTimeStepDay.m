function StaticFrequencyTimeStepDay(Groupe,Data)

T1 = 60.*[8 10 12 14 16];
T2 = 60.*[10 12 14 16 18];
U = unique(Data.Idx);
load RectFit_2.mat
for hour = 1:length(T1)

    figure('Name',['Time step: ' int2str(T1(hour)) '-' int2str(T2(hour)) ', Groupe ' int2str(Groupe)],'units','normalized','outerposition',[0 0 1 1])
    for Day = 1:4
        
        subplot(2,2,Day);hold on
        subplot(2,2,Day);axis image
        I = intersect(intersect(find(Data.Time >= T1(hour)),find(Data.Time <= T2(hour))),...
            intersect(find(Data.Groupe == Groupe),find(Data.Day == Day)));
        
        H = hist(Data.Idx(I),U)./numel(unique(Data.Time(I)));
        
        J = U(H>0)';
        c = H(H>0);
        x = [Data.Cx1(J) ; Data.Cx2(J) ; Data.Cx3(J) ; Data.Cx4(J)];
        y = [Data.Cy1(J) ; Data.Cy2(J) ; Data.Cy3(J) ; Data.Cy4(J)];
        subplot(2,2,Day);patch(x,y,c,'EdgeColor','w')
        C = rot90(rot90(colormap(gray(50))));
        subplot(2,2,Day);colormap(C)
        subplot(2,2,Day);caxis([0 0.3])
        subplot(2,2,Day);colorbar
        
        if Groupe == 1
            subplot(2,2,Day);plot(Data.z10(:,1),Data.z10(:,2))
            subplot(2,2,Day);plot(Data.z11(:,1),Data.z11(:,2))
            subplot(2,2,Day);plot(Data.z12(:,1),Data.z12(:,2))
            subplot(2,2,Day);plot(Data.z13(:,1),Data.z13(:,2))
            subplot(2,2,Day);plot(Data.z14(:,1),Data.z14(:,2))
            subplot(2,2,Day);plot(Data.z15(:,1),Data.z15(:,2))
            subplot(2,2,Day);plot(Data.z16(:,1),Data.z16(:,2))
            subplot(2,2,Day);plot(Data.z17(:,1),Data.z17(:,2))
            subplot(2,2,Day);plot(Data.z18(:,1),Data.z18(:,2))
            subplot(2,2,Day);plot(Data.z19(:,1),Data.z19(:,2))
        end
        if Groupe == 2
            subplot(2,2,Day);plot(Data.z21(:,1),Data.z21(:,2))
            subplot(2,2,Day);plot(Data.z22(:,1),Data.z22(:,2))
            subplot(2,2,Day);plot(Data.z23(:,1),Data.z23(:,2))
            subplot(2,2,Day);plot(Data.z24(:,1),Data.z24(:,2))
            subplot(2,2,Day);plot(Data.z25(:,1),Data.z25(:,2))
            subplot(2,2,Day);plot(Data.z26(:,1),Data.z26(:,2))
            subplot(2,2,Day);plot(Data.z27(:,1),Data.z27(:,2))
        end
        axis off
    end
    drawnow
    frame = getframe(gcf);
    im = frame2im(frame);
    I2 = imcrop(im,rect);
    imwrite(I2,['Time' int2str(T1(hour)/60) '-' int2str(T2(hour)/60) '_Groupe_' int2str(Groupe) '.png'])
    %     print('-dpng',['Day_' int2str(Day) '_Groupe_' int2str(Groupe) '.png'])
end
close all


