function Freq(Data,Week,Groupe,Day)

[A,R] = geotiffread('CarteBase.tif');

% 
% s = 1/0.0614147987920735;
% A_2 = imresize(A,1/s);
% R_2 = R;
% R_2.RasterSize = size(A_2);


switch Week
    case 1
        d1=1;d2=4;
    case 2
        d1=5;d2=8;
    case 3
        d1=9;d2=11;
    otherwise
end

if nargin == 4
    d1 = Day;d2 = Day;
end

g = Groupe;

I = find((Data.Day >= d1)&(Data.Day <= d2)&(Data.Groupe == g));
U = unique(Data.Idx(I));
H = hist(Data.Idx(I),U);

NbPics = sum(Data.NumPics((Data.NumPics(:,1) >= d1)&(Data.NumPics(:,1) <=d2)&(Data.NumPics(:,2) == g),3));

H = (100.*H)./NbPics;
% max(H)
c = zeros(length(H),1,3);
c0 = transpose((8.*ones - H)./8);
c0(c0 > 1) = 1;
c0(c0 < 0) = 0;
c(:,1,1) = c0;
c(:,1,2) = c0;
c(:,1,3) = c0;
% 
% figure('Name',['Groupe ' int2str(Groupe) ', Week ' int2str(Week)])
% mapshow(A,R)
% hold on
% patch(Data.Grid_x(:,U),Data.Grid_y(:,U),c,'EdgeColor','w','FaceAlpha',0.5)


q25 = quantile(H,0.25);
q50 = quantile(H,0.5);
q75 = quantile(H,0.75);

C = colormap(jet(4));

c(H <= q25,1,:) = repmat(reshape(C(1,:),[1 , 1 , 3]),[sum(H <= q25) , 1 , 1]);
c((H > q25)&(H <= q50),1,:) = repmat(reshape(C(2,:),[1 , 1 , 3]),[sum((H > q25)&(H <= q50)) , 1 , 1]);
c((H > q50)&(H <= q75),1,:) = repmat(reshape(C(3,:),[1 , 1 , 3]),[sum((H > q50)&(H <= q75)) , 1 , 1]);
c((H > q75),1,:) = repmat(reshape(C(4,:),[1 , 1 , 3]),[sum(H > q75) , 1 , 1]);
close all
figure('Name',['Groupe ' int2str(Groupe) ', Week ' int2str(Week) ' [Quantile]'],'units','normalized','outerposition',[0 0 1 1])
mapshow(A,R)
hold on
patch(Data.Grid_x(:,U),Data.Grid_y(:,U),c,'EdgeColor','w','FaceAlpha',0.5)

c(H > 0,1,1) = zeros;
c(H <= 0,1,1) = ones;


c(H > 0,1,2) = zeros;
c(H <= 0,1,2) = ones;


c(H > 0,1,3) = zeros;
c(H <= 0,1,3) = ones;

figure('Name',['Groupe ' int2str(Groupe) ', Week ' int2str(Week) ' [Presence]'],'units','normalized','outerposition',[0 0 1 1])
mapshow(A,R)
hold on
patch(Data.Grid_x(:,U),Data.Grid_y(:,U),c,'EdgeColor','w','FaceAlpha',0.5)
% [r , c] = R_2.worldToDiscrete(Data.Coord(I,1),Data.Coord(I,2));
% L = sub2ind(size(A_2),r,c);
% 
% H = hist(L,unique(L));
% 
% M = zeros(size(A_2,1),size(A_2,2));
% NbPics = 0;
% 
% for i = 1:4
%     NbPics = NbPics + numel(Data.Time(find((Data.Day == i)&(Data.Groupe == g))));
% end
% H = (100.*H)./NbPics;
% 
% M(unique(L)) = H;
% M_2 = imresize(M,s,'nearest');
% I = find(M_2 <= 0);



% figure('Name',['Groupe ' int2str(Groupe) ', Week ' int2str(Week)])
% imagesc(A)
% hold on
% h=imagesc(M_2);
% axis image
% alpha = 1.*ones(size(M_2));
% alpha(I) = 0.5;
% set(h,'AlphaData',alpha)
% caxis([0 0.6])
% colormap(rot90(rot90(gray(100))))
% colorbar
% 


% figure('Name',['Max , Groupe ' int2str(Groupe) ', Week ' int2str(Week)])
% imagesc(A)
% hold on
% M_2(M_2 > 0.5) = 1;
% M_2(M_2 < 1) = 0;
% h=imagesc(M_2);
% axis image
% alpha = 10.*ones(size(M_2));
% alpha(I) = 0;
% set(h,'AlphaData',alpha)
% caxis([0 1])
% colormap(rot90(rot90(gray(2))))
% colorbar