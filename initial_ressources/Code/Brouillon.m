load Res.mat
Activity = -ones(numel(Data.Idx),1);
Activity = DetectActivity(Res,1,Activity);
Activity = DetectActivity(Res,2,Activity);

% load Data.mat
% Us = pwd;
% chemin = 'C:\Users\mtbonneau\Documents\Stage\2017\DD_Medhy';
% filename = strvcat('100417','110417','120417','130417','150517','160517','170517','180517','250717','260717','270717');
% % Activity = -ones(numel(Data.Idx),1);
% k = 1;
% Groupe = 2;
% NumError = 0;
% for fn = 1:size(filename,1)
%     Day = fn;
%     
%     
%     IAll = intersect(find(Data.Day == Day),find(Data.Groupe == Groupe));
%     AllTime = unique(Data.Time(IAll));
%     
%     for Num = 1:numel(AllTime)
%         disp(['Jours : ' int2str(Day) ', Num Pic : ' int2str(Num) ', Num Error : ' int2str(NumError)])
%         
%         cd(chemin)
%         cd([filename(fn,:) '\g2\RASTER'])
%         Idx_Init = intersect(IAll,find(Data.Time == AllTime(Num)));
%         N = Data.Coord(Idx_Init,:);
%         
%         [A, R] = geotiffread([int2str(Num) '.tif']);
%         cd(Us);
%         [x , y] = R.worldToDiscrete(N(:,1),N(:,2));
%         
%         [X , Y] = meshgrid(1:41,1:41);
%         
%         
%         for i = 1:length(x)
%             
%             try
%                 d = 20;
%                 J = A((x(i) - d):(x(i) + d),(y(i) - d):(y(i) + d),:);
%                 %             figure
%                 %             subplot(1,2,1);imagesc(J);axis off
%                 
%                 try
%                     Res = Detect(J);
%                     K = convhull(X(Res == 1),Y(Res == 1));
%                     I = find(Res == 1);
%                     Hull = [X(I(K)) , Y(I(K))];
%                     M = Res;
%                     M(inhull([X(:),Y(:)],Hull)) = 1;
%                     stats = regionprops('table',Res,'Eccentricity',...
%                         'Extent','MajorAxisLength','Solidity');
%                     if (stats.Extent < 0.8) && (stats.Extent > 0.2)
%                         Activity(Idx_Init(i)) = stats.Eccentricity;
%                     end
%                 catch
%                     NumError = NumError + 1;
%                 end
%             catch
%                 NumError = NumError + 1;
%             end
%             %             k = k + 1;
%         end
%         
%     end
% end
% 
% 
% 
