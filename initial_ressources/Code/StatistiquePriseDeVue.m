load Data.mat

%% Number of pics
figure
hold on
title('Number of pictures')
%% G1
subplot(2,3,1);hold on
title('G1, Week 1')
xlabel('Day')
ylabel('Number of pics')
bar(1:4,Data.NumPics((Data.NumPics(:,1) <= 4)&(Data.NumPics(:,2) == 1),3))
set(gca,'FontName','TimeNewsRoman','FontSize',16)
subplot(2,3,2);hold on
title('G1, Week 2')
xlabel('Day')
ylabel('Number of pics')
bar(5:8,Data.NumPics((Data.NumPics(:,1) >= 5)&(Data.NumPics(:,1) <= 8)&(Data.NumPics(:,2) == 1),3))
set(gca,'FontName','TimeNewsRoman','FontSize',16)
subplot(2,3,3);hold on
title('G1, Week 3')
xlabel('Day')
ylabel('Number of pics')
bar(9:11,Data.NumPics((Data.NumPics(:,1) >= 9)&(Data.NumPics(:,2) == 1),3))
set(gca,'FontName','TimeNewsRoman','FontSize',16)
%% G2
subplot(2,3,4);hold on
title('G2, Week 1')
xlabel('Day')
ylabel('Number of pics')
bar(1:4,Data.NumPics((Data.NumPics(:,1) <= 4)&(Data.NumPics(:,2) == 2),3))
set(gca,'FontName','TimeNewsRoman','FontSize',16)
subplot(2,3,5);hold on
title('G2, Week 2')
xlabel('Day')
ylabel('Number of pics')
bar(5:8,Data.NumPics((Data.NumPics(:,1) >= 5)&(Data.NumPics(:,1) <= 8)&(Data.NumPics(:,2) == 2),3))
set(gca,'FontName','TimeNewsRoman','FontSize',16)
subplot(2,3,6);hold on
title('G2, Week 3')
xlabel('Day')
ylabel('Number of pics')
bar(9:11,Data.NumPics((Data.NumPics(:,1) >= 9)&(Data.NumPics(:,2) == 2),3))
set(gca,'FontName','TimeNewsRoman','FontSize',16)


%%% Number of detected goats per time step
%% Semaine 1, G1
I = find((Data.Groupe == 1)&(Data.Day <=4));
T = Data.Time(I);
G = zeros(size(T));
G(T<= 10*60) = 1;
G((T > 10*60)&(T <= 12*60)) = 2;
G((T > 12*60)&(T <= 14*60)) = 3;
G((T > 14*60)&(T <= 16*60)) = 4;
G((T > 16*60)) = 5;
figure
subplot(2,3,1);hold on
bh = boxplot(T,G,'Labels',{'10H','12H','14H','16H','+16H'});
hold on
for i = 1:numel(bh)
    set(bh(i),'Linewidth',3')
end
grid on
ylabel('Nombre de ch�vres')
xlabel('Tranche horaire')
title('Week 1')
set(gca,'FontName','TimeNewsRoman','FontSize',16)

%% Semaine 2, G1
I = find((Data.Groupe == 1)&(Data.Day <= 8)&(Data.Day > 4));
T = Data.Time(I);
G = zeros(size(T));
G(T<= 10*60) = 1;
G((T > 10*60)&(T <= 12*60)) = 2;
G((T > 12*60)&(T <= 14*60)) = 3;
G((T > 14*60)&(T <= 16*60)) = 4;
G((T > 16*60)) = 5;
subplot(2,3,2);hold on
bh = boxplot(T,G,'Labels',{'10H','12H','14H','16H','+16H'});
hold on
for i = 1:numel(bh)
    set(bh(i),'Linewidth',3')
end
grid on
% ylabel('Nombre de ch�vres')
xlabel('Tranche horaire')
title('Week 2')
set(gca,'FontName','TimeNewsRoman','FontSize',16)

%% Semaine 3, G1
I = find((Data.Groupe == 1)&(Data.Day <= 11)&(Data.Day > 8));
T = Data.Time(I);
G = zeros(size(T));
G(T<= 10*60) = 1;
G((T > 10*60)&(T <= 12*60)) = 2;
G((T > 12*60)&(T <= 14*60)) = 3;
G((T > 14*60)&(T <= 16*60)) = 4;
G((T > 16*60)) = 5;
subplot(2,3,3);hold on
bh = boxplot(T,G,'Labels',{'10H','12H','14H','16H','+16H'});
hold on
for i = 1:numel(bh)
    set(bh(i),'Linewidth',3')
end
grid on
% ylabel('Nombre de ch�vres')
xlabel('Tranche horaire')
title('Week 3')
set(gca,'FontName','TimeNewsRoman','FontSize',16)


%% Semaine 1, G2
I = find((Data.Groupe == 2)&(Data.Day <=4));
T = Data.Time(I);
G = zeros(size(T));
G(T<= 10*60) = 1;
G((T > 10*60)&(T <= 12*60)) = 2;
G((T > 12*60)&(T <= 14*60)) = 3;
G((T > 14*60)&(T <= 16*60)) = 4;
G((T > 16*60)) = 5;
subplot(2,3,4);hold on
bh = boxplot(T,G,'Labels',{'10H','12H','14H','16H','+16H'});
hold on
for i = 1:numel(bh)
    set(bh(i),'Linewidth',3')
end
grid on
ylabel('Nombre de ch�vres')
xlabel('Tranche horaire')
title('Week 1')
set(gca,'FontName','TimeNewsRoman','FontSize',16)

%% Semaine 2, G2
I = find((Data.Groupe == 2)&(Data.Day <= 8)&(Data.Day > 4));
T = Data.Time(I);
G = zeros(size(T));
G(T<= 10*60) = 1;
G((T > 10*60)&(T <= 12*60)) = 2;
G((T > 12*60)&(T <= 14*60)) = 3;
G((T > 14*60)&(T <= 16*60)) = 4;
G((T > 16*60)) = 5;
subplot(2,3,5);hold on
bh = boxplot(T,G,'Labels',{'10H','12H','14H','16H','+16H'});
hold on
for i = 1:numel(bh)
    set(bh(i),'Linewidth',3')
end
grid on
% ylabel('Nombre de ch�vres')
xlabel('Tranche horaire')
title('Week 2')
set(gca,'FontName','TimeNewsRoman','FontSize',16)

%% Semaine 3, G2
I = find((Data.Groupe == 2)&(Data.Day <= 11)&(Data.Day > 8));
T = Data.Time(I);
G = zeros(size(T));
G(T<= 10*60) = 1;
G((T > 10*60)&(T <= 12*60)) = 2;
G((T > 12*60)&(T <= 14*60)) = 3;
G((T > 14*60)&(T <= 16*60)) = 4;
G((T > 16*60)) = 5;
subplot(2,3,6);hold on
bh = boxplot(T,G,'Labels',{'10H','12H','14H','16H','+16H'});
hold on
for i = 1:numel(bh)
    set(bh(i),'Linewidth',3')
end
grid on
% ylabel('Nombre de ch�vres')
xlabel('Tranche horaire')
title('Week 3')
set(gca,'FontName','TimeNewsRoman','FontSize',16)
