Groupe = 1;

VG1 = zeros(1,11);
for Day = 1:11
    
    A = Data.Activity((Data.Day == Day)&(Data.Groupe == Groupe));
    
    VG1(Day) = sum(A >= 0.9)/sum(A ~=-1);   
end

Groupe = 2;
U = unique(Data.Idx(Data.Groupe == Groupe));

VG2 = zeros(11,length(U));
for Day = 1:11
    
    A = Data.Activity((Data.Day == Day)&(Data.Groupe == Groupe));
    VG2(Day) = sum(A >= 0.9)/sum(A ~=-1);   
end


%%
figure('Name','Week 1','units','normalized','outerposition',[0 0 1 1])
plot(1:4,VG1(1:4),1:4,VG2(1:4),'LineWidth',3)
legend('Parcelle 1','Parcelle 2','Location','northwest')
grid on
ylabel('Proportion s''alimentant')
xlabel('Day')
set(gca,'FontName','TimeNewsRoman','FontSize',22,'XTick',1:4)

%%
figure('Name','Week 2','units','normalized','outerposition',[0 0 1 1])
plot(1:4,VG1(5:8),1:4,VG2(5:8),'LineWidth',3)
legend('Parcelle 1','Parcelle 2','Location','northwest')
grid on
ylabel('Proportion s''alimentant')
xlabel('Day')
set(gca,'FontName','TimeNewsRoman','FontSize',22,'XTick',1:4)

%%
figure('Name','Week 3','units','normalized','outerposition',[0 0 1 1])
   
plot(9:11,VG1(9:11),9:11,VG2(9:11),'LineWidth',3)
legend('Parcelle 1','Parcelle 2','Location','northwest')
grid on
ylabel('Proportion s''alimentant')
xlabel('Day')
set(gca,'FontName','TimeNewsRoman','FontSize',22,'XTick',9:11)
