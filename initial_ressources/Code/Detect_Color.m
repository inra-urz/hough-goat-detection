function Res = Detect_Color(I_Init,Idx_Init)


load Proba_Color_Full.mat

I = double(I_Init);
R = squeeze(I(:,:,1));
G = squeeze(I(:,:,2));
B = squeeze(I(:,:,3));

lab_he = double(rgb2lab(I_Init));
L = R + G + B;
A = round(squeeze(lab_he(:,:,3)));
b = round(squeeze(lab_he(:,:,3)));

lab_he = double(rgb2lin(I_Init));
Lin1 = squeeze(lab_he(:,:,1));
Lin2 = squeeze(lab_he(:,:,3));
Lin3 = squeeze(lab_he(:,:,3));


Varigreen = (squeeze(I(:,:,2)) - squeeze(I(:,:,1)))./...
    (squeeze(I(:,:,2)) + squeeze(I(:,:,1)) - squeeze(I(:,:,3)));

M = G;
Diff_G = (M - mean(mean(M)).*ones)./(var(var(M)));

M = Varigreen;
Diff_V = (M - mean(mean(M)).*ones)./(var(var(M)));

X = [R(:) , G(:) , B(:) , L(:) , A(:) , b(:) , Lin1(:) , Lin2(:) , Lin3(:) , Varigreen(:) ,...
    Diff_G(:) , Diff_V(:)];
X(:,1) = X(:,1) + ones;
X(:,2) = X(:,2) + ones;
X(:,3) = X(:,3) + ones;

X(:,4) = X(:,4) + ones;
X(:,5) = X(:,5) + 301.*ones;
X(:,6) = X(:,6) + 301.*ones;

X(:,7) = X(:,7) + ones;
X(:,8) = X(:,8) + ones;
X(:,9) = X(:,9) + ones;
X(:,10) = round(double(round(X(:,10),2).*100 + 101.*ones));
X(isinf(X(:,10)),10) = 200;
X(isnan(X(:,10)),10) = 200;
X(X(:,10) <= 0,10) = 200;

R1 = [X(:,11) , -ones(size(X,1),1)];
R2 = [ones(1,numel(vG)) ; vG];
R = R1*R2;
R = R <=0;
[~ , Idx] = max(R,[],2);
X(:,11) = Idx;

R1 = [X(:,12) , -ones(size(X,1),1)];
R2 = [ones(1,numel(vV)) ; vV];
R = R1*R2;
R = R <=0;
[~ , Idx] = max(R,[],2);
X(:,12) = Idx;


% P = Proba_R(X(:,1),:).*Proba_G(X(:,2),:).*Proba_B(X(:,3),:).*Proba_L(X(:,4),:).*Proba_a(X(:,5),:)...
%     .*Proba_b(X(:,6),:).*Proba_L1(X(:,7),:).*Proba_L2(X(:,8),:).*Proba_L3(X(:,9),:).*Proba_V(X(:,10));
% 
% P = Proba_R(X(:,1),:) + Proba_G(X(:,2),:) + Proba_B(X(:,3),:) + Proba_L(X(:,4),:) + Proba_a(X(:,5),:)...
%     + Proba_b(X(:,6),:) + Proba_L1(X(:,7),:) + Proba_L2(X(:,8),:) + Proba_L3(X(:,9),:);

% P = Proba_R(X(:,1),:).*Proba_G(X(:,2),:).*Proba_B(X(:,3),:);%.*Proba_V(X(:,10),:);
P = Proba_R(X(:,1),:);

% P = Proba_G(X(:,2),:).*Proba_V(X(:,10),:).*exp(Proba_DiffG(X(:,11))).*exp(Proba_DiffV(X(:,12)));
% P = Proba_G(X(:,2),:).*Proba_V(X(:,10),:);

% P = Proba_L1(X(:,7),:).*Proba_L2(X(:,8),:).*Proba_L3(X(:,9),:);

[~ , Res] = max(P,[],2);
Res = reshape(Res,size(I,1),size(I,2));
Res(Idx_Init) = 0;

a = max(max(Res(Res ~= 0)));
if a == 1
    Res = 'Red';
else
    Res = 'Black';
end

% figure
% Res(1:6) = 1:6;
% imagesc(reshape(Res,size(I,1),size(I,2)))
% colormap([0 0 0 ; 1 1 1 ; 0.5 .5 .5 ; 0 .3 0 ; 0 0.6 0 ; 0 1 0])
% labels = {'Goats','Wood 1','Wood 2','DWeed','AWeed','GWeed'};
% lcolorbar(labels,'fontweight','bold');
% axis image
% figure
% imshow(I_Init)
% cd(ch_Init);
