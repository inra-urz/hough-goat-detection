function Difference(Data,Groupe)

[A,R] = geotiffread('CarteBase.tif');
g = Groupe;
U = unique(Data.Idx(Data.Groupe == g));
%%
Week = 1;
switch Week
    case 1
        d1=1;d2=4;
    case 2
        d1=5;d2=8;
    case 3
        d1=9;d2=11;
    otherwise
end

I = find((Data.Day >= d1)&(Data.Day <= d2)&(Data.Groupe == g));
H = hist(Data.Idx(I),U);

NbPics = sum(Data.NumPics((Data.NumPics(:,1) >= d1)&(Data.NumPics(:,1) <=d2)&(Data.NumPics(:,2) == g),3));

H_W1 = (100.*H)./NbPics;


%%
Week = 2;

switch Week
    case 1
        d1=1;d2=4;
    case 2
        d1=5;d2=8;
    case 3
        d1=9;d2=11;
    otherwise
end


I = find((Data.Day >= d1)&(Data.Day <= d2)&(Data.Groupe == g));
H = hist(Data.Idx(I),U);

NbPics = sum(Data.NumPics((Data.NumPics(:,1) >= d1)&(Data.NumPics(:,1) <=d2)&(Data.NumPics(:,2) == g),3));

H_W2 = (100.*H)./NbPics;



H = abs(H_W1 - H_W2);

m = max(H);
c = zeros(length(H),1,3);
c0 = transpose((m.*ones - H)./m);
c0(c0 > 1) = 1;
c0(c0 < 0) = 0;
c(:,1,1) = c0;
c(:,1,2) = c0;
c(:,1,3) = c0;

figure('Name',['Groupe ' int2str(Groupe) ' [Difference]'],'units','normalized','outerposition',[0 0 1 1])
subplot(1,2,1);
hold on
mapshow(A,R)
patch(Data.Grid_x(:,U),Data.Grid_y(:,U),c,'EdgeColor','w','FaceAlpha',0.5)
subplot(1,2,2);
H = hist(H,linspace(0,m,20))./sum(H);
bar(linspace(0,m,20),H)



%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%
q25 = quantile(H_W1,0.25);
q50 = quantile(H_W1,0.5);
q75 = quantile(H_W1,0.75);

VW1 = zeros(size(H_W1));
VW1(H_W1 <= q25) = ones;
VW1((H_W1 > q25)&(H_W1 <= q50)) = 2.*ones;
VW1((H_W1 > q50)&(H_W1 <= q75)) = 3.*ones;
VW1((H_W1 > q75)) = 4.*ones;

q25 = quantile(H_W2,0.25);
q50 = quantile(H_W2,0.5);
q75 = quantile(H_W2,0.75);

VW2 = zeros(size(H_W1));
VW2(H_W2 <= q25) = ones;
VW2((H_W2 > q25)&(H_W2 <= q50)) = 2.*ones;
VW2((H_W2 > q50)&(H_W2 <= q75)) = 3.*ones;
VW2((H_W2 > q75)) = 4.*ones;

H_1 = hist(VW2(VW1 == 1),1:4)'./sum(VW1 == 1);
H_2 = hist(VW2(VW1 == 2),1:4)'./sum(VW1 == 2);
H_3 = hist(VW2(VW1 == 3),1:4)'./sum(VW1 == 3);
H_4 = hist(VW2(VW1 == 4),1:4)'./sum(VW1 == 4);

table(strvcat('Spontan�e','Faible','Mod�r�e','Forte'),H_1,H_2,H_3,H_4,'VariableNames',{'Week_1','Spontanee','Faible','Moderee','Forte'})








q25 = quantile(H_W1,0.25);
q50 = quantile(H_W1,0.5);
q75 = quantile(H_W1,0.75);

% %%
J = find(H_W1 <= q25);
H = abs(H_W1(J) - H_W2(J));

% m = max(H);
% c = zeros(length(H),1,3);
% c0 = transpose((m.*ones - H)./m);
% c0(c0 > 1) = 1;
% c0(c0 < 0) = 0;
% c(:,1,1) = c0;
% c(:,1,2) = c0;
% c(:,1,3) = c0;
% 
% figure('Name',['Groupe ' int2str(Groupe) ' [Difference, q25]'],'units','normalized','outerposition',[0 0 1 1])
% subplot(1,2,1);
% hold on
% mapshow(A,R)
% patch(Data.Grid_x(:,U(J)),Data.Grid_y(:,U(J)),c,'EdgeColor','w','FaceAlpha',0.5)
% subplot(1,2,2);
H = hist(H,linspace(0,m,20));
H_25 = H./sum(H);
%%
J = find((H_W1 > q25)&(H_W1 <= q50));
H = abs(H_W1(J) - H_W2(J));

% m = max(H);
% c = zeros(length(H),1,3);
% c0 = transpose((m.*ones - H)./m);
% c0(c0 > 1) = 1;
% c0(c0 < 0) = 0;
% c(:,1,1) = c0;
% c(:,1,2) = c0;
% c(:,1,3) = c0;
% 
% figure('Name',['Groupe ' int2str(Groupe) ' [Difference, q50]'],'units','normalized','outerposition',[0 0 1 1])
% subplot(1,2,1);
% hold on
% mapshow(A,R)
% patch(Data.Grid_x(:,U(J)),Data.Grid_y(:,U(J)),c,'EdgeColor','w','FaceAlpha',0.5)
% subplot(1,2,2);
% hist(H./sum(H),linspace(0,m,20))
H = hist(H,linspace(0,m,20));
H_50 = H./sum(H);
%%
J = find((H_W1 > q50)&(H_W1 <= q75));
H = abs(H_W1(J) - H_W2(J));

% m = max(H);
% c = zeros(length(H),1,3);
% c0 = transpose((m.*ones - H)./m);
% c0(c0 > 1) = 1;
% c0(c0 < 0) = 0;
% c(:,1,1) = c0;
% c(:,1,2) = c0;
% c(:,1,3) = c0;
% 
% figure('Name',['Groupe ' int2str(Groupe) ' [Difference, q50]'],'units','normalized','outerposition',[0 0 1 1])
% subplot(1,2,1);
% hold on
% mapshow(A,R)
% patch(Data.Grid_x(:,U(J)),Data.Grid_y(:,U(J)),c,'EdgeColor','w','FaceAlpha',0.5)
% subplot(1,2,2);
% hist(H./sum(H),linspace(0,m,20))
H = hist(H,linspace(0,m,20));
H_75 = H./sum(H);
%%
J = find((H_W1 > q75));
H = abs(H_W1(J) - H_W2(J));

% m = max(H);
% c = zeros(length(H),1,3);
% c0 = transpose((m.*ones - H)./m);
% c0(c0 > 1) = 1;
% c0(c0 < 0) = 0;
% c(:,1,1) = c0;
% c(:,1,2) = c0;
% c(:,1,3) = c0;
% 
% figure('Name',['Groupe ' int2str(Groupe) ' [Difference, q75]'],'units','normalized','outerposition',[0 0 1 1])
% subplot(1,2,1);
% hold on
% mapshow(A,R)
% patch(Data.Grid_x(:,U(J)),Data.Grid_y(:,U(J)),c,'EdgeColor','w','FaceAlpha',0.5)
% subplot(1,2,2);
% hist(H./sum(H),linspace(0,m,20))
figure('Name',['Groupe ' int2str(Groupe) ' [Difference par classe]'],'units','normalized','outerposition',[0 0 1 1])
H = hist(H,linspace(0,m,20));
H_100 = H./sum(H);
bar(linspace(0,m,20),[H_25 ; H_50 ; H_75 ; H_100]')
legend('Spontann�e','Faible','Mod�r�e','Forte')
grid on
ylabel('Fr�quence')
xlabel('Classe de diff�rence')
set(gca,'FontName','TimeNewsRoman','FontSize',22)
%%
J_W1 = find((H_W1 > q75));
q75 = quantile(H_W2,0.75);
J_W2 = find((H_W2 > q75));
J = intersect(J_W1,J_W2);

c = zeros(length(J),1,3);
c(:,1,1) = ones;

figure('Name',['Groupe ' int2str(Groupe) ' [Fort both]'],'units','normalized','outerposition',[0 0 1 1])
hold on
mapshow(A,R)
patch(Data.Grid_x(:,U(J)),Data.Grid_y(:,U(J)),c,'EdgeColor','w','FaceAlpha',0.5)
axis image