function Activity = DetectActivity(Res,Groupe,Activity)

Data = struct('Day',Res(:,1),'Groupe',Res(:,2),'Time',Res(:,3));

Us = pwd;
chemin = 'C:\Users\mtbonneau\Documents\Stage\2017\DD_Medhy';
filename = strvcat('100417','110417','120417','130417','150517','160517','170517','180517','250717','260717','270717');
% Activity = -ones(numel(Data.Idx),1);
NumError = 0;
for fn = 1:size(filename,1)
    Day = fn;
    
    
    IAll = intersect(find(Data.Day == Day),find(Data.Groupe == Groupe));
    AllTime = unique(Data.Time(IAll));
    
    for Num = 1:numel(AllTime)
        disp(['Jours : ' int2str(Day) ', Num Pic : ' int2str(Num) ', Num Error : ' int2str(NumError)])
        
        cd(chemin)
        cd([filename(fn,:) '\g' int2str(Groupe) '\RASTER'])
        Idx_Init = intersect(IAll,find(Data.Time == AllTime(Num)));
        X = Res(Idx_Init,4);
        Y = Res(Idx_Init,5);
        
        if X(1) ~= -1
            [A , R] = geotiffread([int2str(Num) '.tif']);
            
            if R.XWorldLimits(1) > 0
                
                cd(Us)
                [x1 , y1 , UN1 , UN2] = wgs2utm(Y,X,20,'N');
                %             cd(chemin)
                X = x1;
                Y = y1;
            end
            
            cd(Us);
            [x , y] = R.worldToDiscrete(X,Y);
            
            [X , Y] = meshgrid(1:41,1:41);
            
            
            for i = 1:length(x)
                
                try
                    d = 20;
                    J = A((x(i) - d):(x(i) + d),(y(i) - d):(y(i) + d),:);
                    %             figure
                    %             subplot(1,2,1);imagesc(J);axis off
                    
                    try
                        Res_2 = Detect(J);
%                         figure
%                         subplot(1,2,1);imshow(J);
%                         subplot(1,2,2);imagesc(Res_2);
%                         pause(2)
%                         close all
                        K = convhull(X(Res_2 == 1),Y(Res_2 == 1));
                        I = find(Res_2 == 1);
                        Hull = [X(I(K)) , Y(I(K))];
                        M = Res_2;
                        M(inhull([X(:),Y(:)],Hull)) = 1;
                        stats = regionprops('table',Res_2,'Eccentricity',...
                            'Extent','MajorAxisLength','Solidity');
                        if (stats.Extent < 0.8) && (stats.Extent > 0.2)
                            Activity(Idx_Init(i)) = stats.Eccentricity;
                        end
                    catch
                        NumError = NumError + 1;
                    end
                catch
                    NumError = NumError + 1;
                end
                %             k = k + 1;
            end
        end
    end
end
