load Data.mat
%% Static map of frequency
StaticFrequency(1,Data,[1 4])
StaticFrequency(1,Data,[5 8])
StaticFrequency(1,Data,[9 11])
StaticFrequency(2,Data,[1 4])
StaticFrequency(2,Data,[5 8])
StaticFrequency(2,Data,[9 11])


%% Histogram of Frequency
ShowHistFreq(Data,2)

figure
hold on
[~ , I] = max(Data.Coord,[],1); 
plot(Data.Coord(I(1),1),Data.Coord(I(1),2),'o')
plot(Data.Coord(I(2),1),Data.Coord(I(2),2),'o')
for i = 1:length(Data.Coord)
    text(Data.Coord(i,1),Data.Coord(i,2),num2str(Data.Idx(i)))
end













Groupe = 2;

U = unique(Data.Idx);
for Day = 1:4
    
    figure('Name',['Day ' int2str(Day) ', Groupe ' int2str(Groupe)],'units','normalized','outerposition',[0 0 1 1])
    hold on
    axis image
    I = intersect(find(Data.Day == Day),find(Data.Groupe == Groupe));
    Time = unique(Data.Time(I));
    H = zeros(1,length(U));
    
    for i = 1:numel(Time)
        
        F = find(Data.Time(I) == Time(i));
        h = hist(Data.Idx(I(F)),U);
        h(h>0) = ones;
        H = H + h;
        
    end
    H = H./numel(Time);
    
    J = U(H>0)';
    c = H(H>0);
    x = [Data.Cx1(J) ; Data.Cx2(J) ; Data.Cx3(J) ; Data.Cx4(J)];
    y = [Data.Cy1(J) ; Data.Cy2(J) ; Data.Cy3(J) ; Data.Cy4(J)];
    patch(x,y,c,'EdgeColor','w')
    C = rot90(rot90(colormap(gray(50))));
    colormap(C)
    caxis([0 0.3])
    colorbar
    
    plot(Data.z10(:,1),Data.z10(:,2))
    plot(Data.z11(:,1),Data.z11(:,2))
    plot(Data.z12(:,1),Data.z12(:,2))
    plot(Data.z13(:,1),Data.z13(:,2))
    plot(Data.z14(:,1),Data.z14(:,2))
    plot(Data.z15(:,1),Data.z15(:,2))
    plot(Data.z16(:,1),Data.z16(:,2))
    plot(Data.z17(:,1),Data.z17(:,2))
    plot(Data.z18(:,1),Data.z18(:,2))
    plot(Data.z19(:,1),Data.z19(:,2))
    
    plot(Data.z21(:,1),Data.z21(:,2))
    plot(Data.z22(:,1),Data.z22(:,2))
    plot(Data.z23(:,1),Data.z23(:,2))
    plot(Data.z24(:,1),Data.z24(:,2))
    plot(Data.z25(:,1),Data.z25(:,2))
    plot(Data.z26(:,1),Data.z26(:,2))
    plot(Data.z27(:,1),Data.z27(:,2))
    axis off
    drawnow
    frame = getframe(gcf);
    im = frame2im(frame);
    I2 = imcrop(im,rect);
    imwrite(I2,['Day_' int2str(Day) '_Groupe_' int2str(Groupe) '.png'])
%     print('-dpng',['Day_' int2str(Day) '_Groupe_' int2str(Groupe) '.png'])
end

close all





%% Animated frequency map

Groupe = 1;

U = unique(Data.Idx);
for Day = 1:4
    filename = ['Day_' int2str(Day) '_Groupe_' int2str(Groupe) '.gif'];
    n = 1;
    figure('Name',['Day ' int2str(Day) ', Groupe ' int2str(Groupe)])
    hold on
    axis image
    I = intersect(find(Data.Day == Day),find(Data.Groupe == Groupe));
    Time = unique(Data.Time(I));
    H = zeros(1,length(U));
    
    for i = 1:numel(Time)

        hold on
        axis image
        axis off
        title([int2str(floor(Time(i)/60)) 'H' int2str(Time(i) - 60*floor(Time(i)/60))]) 
        set(gca,'FontName','TimeNewsRoman','FontSize',20)
        F = find(Data.Time(I) == Time(i));
        h = hist(Data.Idx(I(F)),U);
        h(h>0) = ones;
        H = H + h;
        
        H2 = H./numel(Time);
        
        J = U(H>0)';
        c = H2(H>0);
        x = [Data.Cx1(J) ; Data.Cx2(J) ; Data.Cx3(J) ; Data.Cx4(J)];
        y = [Data.Cy1(J) ; Data.Cy2(J) ; Data.Cy3(J) ; Data.Cy4(J)];
        patch(x,y,c,'EdgeColor','w')
        C = rot90(rot90(colormap(gray(50))));
        colormap(C)
        caxis([0 0.3])
        colorbar
        
        plot(Data.z10(:,1),Data.z10(:,2))
        plot(Data.z11(:,1),Data.z11(:,2))
        plot(Data.z12(:,1),Data.z12(:,2))
        plot(Data.z13(:,1),Data.z13(:,2))
        plot(Data.z14(:,1),Data.z14(:,2))
        plot(Data.z15(:,1),Data.z15(:,2))
        plot(Data.z16(:,1),Data.z16(:,2))
        plot(Data.z17(:,1),Data.z17(:,2))
        plot(Data.z18(:,1),Data.z18(:,2))
        plot(Data.z19(:,1),Data.z19(:,2))
        
        plot(Data.z21(:,1),Data.z21(:,2))
        plot(Data.z22(:,1),Data.z22(:,2))
        plot(Data.z23(:,1),Data.z23(:,2))
        plot(Data.z24(:,1),Data.z24(:,2))
        plot(Data.z25(:,1),Data.z25(:,2))
        plot(Data.z26(:,1),Data.z26(:,2))
        plot(Data.z27(:,1),Data.z27(:,2))
        
        drawnow
        frame = getframe(gcf);
        im = frame2im(frame);
        [imind,cm] = rgb2ind(im,256);
        
        if n == 1;
            imwrite(imind,cm,filename,'gif', 'Loopcount',inf);
        else
            %           if t == 2016
            %               for l = 1:10
            %                 imwrite(imind,cm,filename,'gif','WriteMode','append');
            %               end
            %           else
            for l = 1:2
                imwrite(imind,cm,filename,'gif','WriteMode','append');
            end
            %           end
            
        end
        clf
        n = n + 1;
        
    end
end
close all








%% Static map of frequency All Data
Groupe = 1;

U = unique(Data.Idx);

figure('Name',['Groupe ' int2str(Groupe)],'units','normalized','outerposition',[0 0 1 1])
hold on
axis image
I = find(Data.Groupe == Groupe);
Time = unique(Data.Time(I));
H = zeros(1,length(U));

for i = 1:numel(Time)
    
    F = find(Data.Time(I) == Time(i));
    h = hist(Data.Idx(I(F)),U);
    h(h>0) = ones;
    H = H + h;
    
end
H = H./numel(Time);

J = U(H>0)';
c = H(H>0);
x = [Data.Cx1(J) ; Data.Cx2(J) ; Data.Cx3(J) ; Data.Cx4(J)];
y = [Data.Cy1(J) ; Data.Cy2(J) ; Data.Cy3(J) ; Data.Cy4(J)];
patch(x,y,c,'EdgeColor','w')
C = rot90(rot90(colormap(gray(50))));
colormap(C)
caxis([0 0.3])
colorbar

if Groupe == 1
    plot(Data.z10(:,1),Data.z10(:,2))
    plot(Data.z11(:,1),Data.z11(:,2))
    plot(Data.z12(:,1),Data.z12(:,2))
    plot(Data.z13(:,1),Data.z13(:,2))
    plot(Data.z14(:,1),Data.z14(:,2))
    plot(Data.z15(:,1),Data.z15(:,2))
    plot(Data.z16(:,1),Data.z16(:,2))
    plot(Data.z17(:,1),Data.z17(:,2))
    plot(Data.z18(:,1),Data.z18(:,2))
    plot(Data.z19(:,1),Data.z19(:,2))
end
if Groupe ==2
    plot(Data.z21(:,1),Data.z21(:,2))
    plot(Data.z22(:,1),Data.z22(:,2))
    plot(Data.z23(:,1),Data.z23(:,2))
    plot(Data.z24(:,1),Data.z24(:,2))
    plot(Data.z25(:,1),Data.z25(:,2))
    plot(Data.z26(:,1),Data.z26(:,2))
    plot(Data.z27(:,1),Data.z27(:,2))
end
axis off
drawnow
frame = getframe(gcf);
im = frame2im(frame);
I2 = imcrop(im,rect);
imwrite(I2,['Groupe_' int2str(Groupe) '.png'])
%     print('-dpng',['Day_' int2str(Day) '_Groupe_' int2str(Groupe) '.png'])








%%%% Density

Freq = ComputeFrequencePerZone(Data);

Groupe = 1;

figure('Name','Groupe 1','units','normalized','outerposition',[0 0 1 1])
for Day = 1:4
    
    subplot(2,2,Day);bh = boxplot(Freq(intersect(find(Freq(:,1) == Day),find(Freq(:,2) == Groupe)),4:13),'Labels',{'Z0','Z1','Z2','Z3','Z4','Z5','Z6','Z7','Z8','Z9'});
    for i = 1:numel(bh)
        set(bh(i),'Linewidth',3')
    end
    grid on
    title(['Day ' int2str(Day)])
    set(gca,'FontSize',18)
%     p=subplot(2,2,Day);set(p,'Fontsize',18)
end

Groupe = 2;

figure('Name','Groupe 2','units','normalized','outerposition',[0 0 1 1])
for Day = 1:4
    
    subplot(2,2,Day);bh = boxplot(Freq(intersect(find(Freq(:,1) == Day),find(Freq(:,2) == Groupe)),14:end),'Labels',{'Z1','Z2','Z3','Z4','Z5','Z6','Z7'});
        for i = 1:numel(bh)
        set(bh(i),'Linewidth',3')
    end
    grid on
    title(['Day ' int2str(Day)])
    set(gca,'FontSize',18)
%     p=subplot(2,2,Day);set(p,'Fontsize',18)
end



Groupe = 1;

figure('Name','Groupe 1')
M = zeros(4,10);
for Day = 1:4
    
    I = intersect(find(Freq(:,1) == Day),find(Freq(:,2) == Groupe));
    cpt = 0;
    for z = 10:19
        M(Day,cpt + 1) = mean(Freq(I,4+cpt));
        cpt = cpt + 1;
    end
end
t = 1:4;
plot(t,M(:,1),'k--',t,M(:,2),'k+-',t,M(:,3),'k*-',t,M(:,4),'k.-',t,M(:,5),...
    'ks-',t,M(:,6),'m',t,M(:,7),'r',t,M(:,8),'g',t,M(:,9),'y',t,M(:,10),'c','LineWidth',3,'MarkerSize',12)
legend('z0','z1','z2','z3','z4','z5','z6','z7','z8','z9')
grid on
xlabel('Day')
ylabel('Average Density')
set(gca,'FontSize',18,'XTick',1:4)


Groupe = 2;

figure('Name','Groupe 2')
M = zeros(4,7);
for Day = 1:4
    
    I = intersect(find(Freq(:,1) == Day),find(Freq(:,2) == Groupe));
    cpt = 0;
    for z = 21:27
        M(Day,cpt + 1) = mean(Freq(I,14+cpt));
        cpt = cpt + 1;
    end
end
plot(M,'LineWidth',3)
legend('z1','z2','z3','z4','z5','z6','z7')
grid on
xlabel('Day')
ylabel('Average Density')
set(gca,'FontSize',18,'XTick',1:4)



%%% Herbometre
% Herbo, 
% c1, day, 
% c2, z number, 
% c3, groupe, 
% c4 hauteur.
load Herbo.mat
Herbo = Herbo(:,1:4);

Groupe = 1;

figure('Name','Groupe 1')
M = zeros(4,10);
for Day = 1:4
    
    I = intersect(find(Herbo(:,1) == Day),find(Herbo(:,3) == Groupe));
    
    for z = 0:9
        
        J = intersect(I,find(Herbo(:,2) == z));
        M(Day,z + 1) = mean(Herbo(J,4));
    end
end
t = 1:4;
plot(t,M(:,1),'k--',t,M(:,2),'k+-',t,M(:,3),'k*-',t,M(:,4),'k.-',t,M(:,5),...
    'ks-',t,M(:,6),'m',t,M(:,7),'r',t,M(:,8),'g',t,M(:,9),'y',t,M(:,10),'c','LineWidth',3,'MarkerSize',12)
legend('z0','z1','z2','z3','z4','z5','z6','z7','z8','z9')
grid on
xlabel('Day')
ylabel('Average grass height')
set(gca,'FontSize',18)




Groupe = 2;

figure('Name','Groupe 2')
M = zeros(4,7);
for Day = 1:4
    
    I = intersect(find(Herbo(:,1) == Day),find(Herbo(:,3) == Groupe));
    
    for z = 0:9
        
        J = intersect(I,find(Herbo(:,2) == z));
        M(Day,z + 1) = mean(Herbo(J,4));
    end
end
plot(M,'LineWidth',3)
legend('z1','z2','z3','z4','z5','z6','z7')
grid on
xlabel('Day')
ylabel('Average grass height')
set(gca,'FontSize',18)






%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%

%
% Day = 1;
% Groupe = 1;
%
% n = 1;
%
%
% I = intersect(find(Data.Groupe == Day),find(Data.Day == Groupe));
% Time = unique(Data.Time(I));
% D = cell(numel(Time),1);
% M = zeros(size(Data.ZNumber));
% M(Data.ZNumber == -1) = -ones;
% M = flip(M);
% for i = 1:numel(Time)
%
%     F = find(Data.Time(I) == Time(i));
%     for j = 1:length(F)
%         M(Data.Idx(I(F(j)))) = M(Data.Idx(I(F(j)))) + 1;
%     end
%     D{i,1} = M;
% end
% m = max(max((M)));
%
% C = rot90(rot90(colormap(gray(m))));
% C = [1 0 0 ; 1 1 1 ; C];
%
% figure
% clf
%
% for t = 1:size(D,1)
%
%     %     figure('units','normalized','outerposition',[0 0 1 1])
%     M = flip(D{t,1});
%     M(1,1) = m;
%     imagesc(M)
%     axis image
%     colormap(C);
%     drawnow
%     frame = getframe(1);
%     im = frame2im(frame);
%     [imind,cm] = rgb2ind(im,256);
%     if n == 1;
%         imwrite(imind,cm,filename,'gif', 'Loopcount',inf);
%     else
%         %           if t == 2016
%         %               for l = 1:10
%         %                 imwrite(imind,cm,filename,'gif','WriteMode','append');
%         %               end
%         %           else
%         for l = 1:1
%             imwrite(imind,cm,filename,'gif','WriteMode','append');
%         end
%         %           end
%
%     end
%     clf
%     n = n + 1;
% end
%
%
%
% [X , Y] = meshgrid(min(Data.x):max(Data.x),min(Data.y):max(Data.y));



%     if n == 1;
%
%         imagesc(M)
% %         colormap(C)
%         colormap(gray)
%         colorbar
%         drawnow
%         frame = getframe(1);
%         im = frame2im(frame);
%         [imind,cm] = rgb2ind(im,256);
%         imwrite(imind,cm,filename,'gif', 'Loopcount',inf);
%     else
%         %           if t == 2016
%         %               for l = 1:10
%         %                 imwrite(imind,cm,filename,'gif','WriteMode','append');
%         %               end
%         %           else
%         clf
%         F = find(Data.Time(I) == Time(i));
%         for j = 1:length(F)
%             M(Data.Idx(I(F(j)))) = M(Data.Idx(I(F(j)))) + 1;
%         end
%         frame = getframe(1);
%         im = frame2im(frame);
%         [imind,cm] = rgb2ind(im,256);
%         imagesc(M)
% %         colormap(C)
%         colormap(gray)
%         colorbar
%         drawnow
%
%         for l = 1:1
%             imwrite(imind,cm,filename,'gif','WriteMode','append');
%         end
%         %           end
%
%     end
%     clf
%     n = n + 1;
% end
%
%
%
% Groupe = 2;
% figure
% hold on
% U = unique(Data.Idx);
% for Day = 1:4
%
%     subplot(2,2,Day);hold on
%     subplot(2,2,Day);axis image
%     I = intersect(find(Data.Day == Day),find(Data.Groupe == Groupe));
%     Time = unique(Data.Time(I));
%     H = zeros(1,length(U));
%
%     for i = 1:numel(Time)
%
%         F = find(Data.Time(I) == Time(i));
%         h = hist(Data.Idx(I(F)),U);
%         h(h>0) = ones;
%         H = H + h;
%
%     end
%     H = H./numel(Time);
%
%     J = U(H>0)';
%     c = H(H>0);
%     x = [Data.Cx1(J) ; Data.Cx2(J) ; Data.Cx3(J) ; Data.Cx4(J)];
%     y = [Data.Cy1(J) ; Data.Cy2(J) ; Data.Cy3(J) ; Data.Cy4(J)];
%     subplot(2,2,Day);patch(x,y,c)
%     C = rot90(rot90(colormap(gray(50))));
%     subplot(2,2,Day);colormap(C)
%     subplot(2,2,Day);caxis([0 0.3])
%     subplot(2,2,Day);colorbar
%
%     subplot(2,2,Day);plot(Data.z10(:,1),Data.z10(:,2))
%     subplot(2,2,Day);plot(Data.z11(:,1),Data.z11(:,2))
%     subplot(2,2,Day);plot(Data.z12(:,1),Data.z12(:,2))
%     subplot(2,2,Day);plot(Data.z13(:,1),Data.z13(:,2))
%     subplot(2,2,Day);plot(Data.z14(:,1),Data.z14(:,2))
%     subplot(2,2,Day);plot(Data.z15(:,1),Data.z15(:,2))
%     subplot(2,2,Day);plot(Data.z16(:,1),Data.z16(:,2))
%     subplot(2,2,Day);plot(Data.z17(:,1),Data.z17(:,2))
%     subplot(2,2,Day);plot(Data.z18(:,1),Data.z18(:,2))
%     subplot(2,2,Day);plot(Data.z19(:,1),Data.z19(:,2))
%
%     subplot(2,2,Day);plot(Data.z21(:,1),Data.z21(:,2))
%     subplot(2,2,Day);plot(Data.z22(:,1),Data.z22(:,2))
%     subplot(2,2,Day);plot(Data.z23(:,1),Data.z23(:,2))
%     subplot(2,2,Day);plot(Data.z24(:,1),Data.z24(:,2))
%     subplot(2,2,Day);plot(Data.z25(:,1),Data.z25(:,2))
%     subplot(2,2,Day);plot(Data.z26(:,1),Data.z26(:,2))
%     subplot(2,2,Day);plot(Data.z27(:,1),Data.z27(:,2))
%
%
%
% end
