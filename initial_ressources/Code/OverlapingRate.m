function OverlapingRate(Data)

Groupe = 1;
U = unique(Data.Idx(Data.Groupe == Groupe));

VG1 = zeros(11,length(U));
for Day = 1:11
    
    VG1(Day,unique(Data.Idx((Data.Day == Day)&(Data.Groupe == Groupe)))) = ones;   
end

Groupe = 2;
U = unique(Data.Idx(Data.Groupe == Groupe));

VG2 = zeros(11,length(U));
for Day = 1:11
    
    VG2(Day,unique(Data.Idx((Data.Day == Day)&(Data.Groupe == Groupe)))) = ones;   
end

%%
figure('Name','Week 1','units','normalized','outerposition',[0 0 1 1])
O = zeros(2,3);
k = 1;
for Day = 2:4
    
    I = intersect(find(VG1(Day-1,:)),find(VG1(Day,:)));
    O(1,k) = 100*(numel(I)/sum(VG1(Day,:) > 0));
    
    I = intersect(find(VG2(Day-1,:)),find(VG2(Day,:)));
    O(2,k) = 100*(numel(I)/sum(VG2(Day,:) > 0));
    k = k + 1;
end
   
plot(2:4,O(1,:),2:4,O(2,:),'LineWidth',3)
legend('Flock F1','Flock F2')
grid on
ylabel('Overlapping pecentage')
xlabel('Day')
set(gca,'FontName','TimeNewsRoman','FontSize',16,'XTick',2:4)

%%
figure('Name','Week 2','units','normalized','outerposition',[0 0 1 1])
O = zeros(2,3);
k = 1;
for Day = 6:8
    
    I = intersect(find(VG1(Day-1,:)),find(VG1(Day,:)));
    O(1,k) = 100*(numel(I)/sum(VG1(Day,:) > 0));
    
    I = intersect(find(VG2(Day-1,:)),find(VG2(Day,:)));
    O(2,k) = 100*(numel(I)/sum(VG2(Day,:) > 0));
    
    k = k + 1;
end
   
plot(2:4,O(1,:),2:4,O(2,:),'LineWidth',3)
legend('Flock F1','Flock F2')
grid on
ylabel('Overlapping pecentage')
xlabel('Day')
set(gca,'FontName','TimeNewsRoman','FontSize',16,'XTick',2:4)

%%
figure('Name','Week 3','units','normalized','outerposition',[0 0 1 1])
O = zeros(2,2);
k = 1;
for Day = 10:11    
    
    I = intersect(find(VG1(Day-1,:)),find(VG1(Day,:)));
    O(1,k) = 100*(numel(I)/sum(VG1(Day,:) > 0));
    
    I = intersect(find(VG2(Day-1,:)),find(VG2(Day,:)));
    O(2,k) = 100*(numel(I)/sum(VG2(Day,:) > 0));
    k = k + 1;
end
   
plot(3:4,O(1,:),3:4,O(2,:),'LineWidth',3)
legend('Flock F1','Flock F2')
grid on
ylabel('Overlapping pecentage')
xlabel('Day')
set(gca,'FontName','TimeNewsRoman','FontSize',16,'XTick',2:4)


%% Number of visited sites all previous days

%%
figure('Name','Week 1, all days','units','normalized','outerposition',[0 0 1 1])
O = zeros(2,3);
k = 1;
for Day = 2:4
    
    I = intersect(find(VG1(1:(Day-1),:)),find(VG1(Day,:)));
    O(1,k) = 100*(numel(I)/sum(VG1(Day,:) > 0));
    
    I = intersect(find(VG2(1:(Day-1),:)),find(VG2(Day,:)));
    O(2,k) = 100*(numel(I)/sum(VG2(Day,:) > 0));
    k = k + 1;
end
   
plot(2:4,O(1,:),2:4,O(2,:),'LineWidth',3)
legend('Flock F1','Flock F2')
grid on
ylabel('Overlapping pecentage')
xlabel('Day')
set(gca,'FontName','TimeNewsRoman','FontSize',16,'XTick',2:4)

%%
figure('Name','Week 2, all days','units','normalized','outerposition',[0 0 1 1])
O = zeros(2,3);
k = 1;
for Day = 6:8
    
    I = intersect(find(VG1(5:(Day-1),:)),find(VG1(Day,:)));
    O(1,k) = 100*(numel(I)/sum(VG1(Day,:) > 0));
    
    I = intersect(find(VG2(5:(Day-1),:)),find(VG2(Day,:)));
    O(2,k) = 100*(numel(I)/sum(VG2(Day,:) > 0));
    
    k = k + 1;
end
   
plot(2:4,O(1,:),2:4,O(2,:),'LineWidth',3)
legend('Flock F1','Flock F2')
grid on
ylabel('Overlapping pecentage')
xlabel('Day')
set(gca,'FontName','TimeNewsRoman','FontSize',16,'XTick',2:4)

%%
figure('Name','Week 3, all days','units','normalized','outerposition',[0 0 1 1])
O = zeros(2,2);
k = 1;
for Day = 10:11    
    
    I = intersect(find(VG1(9:(Day-1),:)),find(VG1(Day,:)));
    O(1,k) = 100*(numel(I)/sum(VG1(Day,:) > 0));
    
    I = intersect(find(VG2(9:(Day-1),:)),find(VG2(Day,:)));
    O(2,k) = 100*(numel(I)/sum(VG2(Day,:) > 0));
    k = k + 1;
end
   
plot(3:4,O(1,:),3:4,O(2,:),'LineWidth',3)
legend('Flock F1','Flock F2')
grid on
ylabel('Overlapping pecentage')
xlabel('Day')
set(gca,'FontName','TimeNewsRoman','FontSize',16,'XTick',2:4)