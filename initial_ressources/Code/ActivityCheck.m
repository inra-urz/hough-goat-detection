function ActivityCheck(NbCheck,Res)

Data = struct('Day',Res(:,1),'Groupe',Res(:,2),'Time',Res(:,3));

Us = pwd;
chemin = 'C:\Users\mtbonneau\Documents\Stage\2017\DD_Medhy';
filename = strvcat('100417','110417','120417','130417','150517','160517','170517','180517','250717','260717','270717');
% Activity = -ones(numel(Data.Idx),1);
NumError = 0;
for n = 1:NbCheck
    
    fn = randperm(size(filename,1));
    fn = fn(1);
    Day = fn;
    Groupe = randperm(2);
    Groupe = 2;
    
    
    IAll = intersect(find(Data.Day == Day),find(Data.Groupe == Groupe));
    AllTime = unique(Data.Time(IAll));
    AllTime = AllTime(AllTime < 17*60);
    Num = randperm(numel(AllTime));
    Num = Num(1);
    
    cd(chemin)
    cd([filename(fn,:) '\g' int2str(Groupe) '\RASTER'])
    Idx_Init = intersect(IAll,find(Data.Time == AllTime(Num)));
    X = Res(Idx_Init,4);
    Y = Res(Idx_Init,5);
    
    if X(1) ~= -1
        [A , R] = geotiffread([int2str(Num) '.tif']);
        
        if R.XWorldLimits(1) > 0
            
            cd(Us)
            [x1 , y1 , UN1 , UN2] = wgs2utm(Y,X,20,'N');
            %             cd(chemin)
            X = x1;
            Y = y1;
        end
        
        cd(Us);
        [x , y] = R.worldToDiscrete(X,Y);
        
        [X , Y] = meshgrid(1:41,1:41);
        
        
        i = randperm(length(x));
        i = i(1);
        
        try
            d = 20;
            J = A((x(i) - d):(x(i) + d),(y(i) - d):(y(i) + d),:);
            %             figure
            %             subplot(1,2,1);imagesc(J);axis off
            
            try
                Res_2 = Detect(J);
                K = convhull(X(Res_2 == 1),Y(Res_2 == 1));
                I = find(Res_2 == 1);
                Hull = [X(I(K)) , Y(I(K))];
                M = Res_2;
                M(inhull([X(:),Y(:)],Hull)) = 1;
                stats = regionprops('table',Res_2,'Eccentricity',...
                    'Extent','MajorAxisLength','Solidity');
                if (stats.Extent < 0.8) && (stats.Extent > 0.2)
                    %                         Activity(Idx_Init(i)) = stats.Eccentricity;
                    figure
                    subplot(1,2,1);imshow(J);
                    subplot(1,2,2);hold on;imagesc(flip(Res_2));
                    axis image
                    if stats.Eccentricity > 0.9
                        title('Eating')
                    else
                        title('Sleeping')
                    end
%                     pause
%                     close all
                end
            catch
                NumError = NumError + 1;
            end
        catch
            NumError = NumError + 1;
        end
        %             k = k + 1;
        
    end
end