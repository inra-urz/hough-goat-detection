M = rgb2gray(imread('I.jpg'));  %% Read image I.jpg and transform to black and white
Z = imsubtract(M_1,M_2); %% Substract image M_2 to M_1
imshow(Z) %% plot image Z
M = double(imread('I.jpg')); %% When image are uploaded, it's a n by p by 3 matrix. n by p is the image size and third component is for Red Green and Blue. 
                             %% The matrix is initially of uint8 type. Function double(.) allows to convert into data type double. 
                             
%% Exemple of the kmean method                         
Z = imread('I.jpg');

cform = makecform('srgb2lab');
lab_he = applycform(Z,cform);
ab = double(lab_he(:,:,2:3));
nrows = size(ab,1);
ncols = size(ab,2);
ab = reshape(ab,nrows*ncols,2);

nColors = 2; %% Number of cluster
% repeat the clustering 3 times to avoid local minima
[cluster_idx, cluster_center] = kmeans(ab,nColors,'distance','sqeuclidean', ...
    'Replicates',3);
pixel_labels = reshape(cluster_idx,nrows,ncols);

imagesc(reshape(pixel_labels,[size(Z,1) , size(Z,2)]))
axis image

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% There is not just RGB to describe an image but tones of stuff, here are some examples
I = imread('I.jpg');

figure('name','Somme RGB')
imagesc(sum(I,3))
axis image

figure('name','Prod RGB')
imagesc(prod(I,3))
axis image
colorbar

figure('name','Red')
imagesc(squeeze(I(:,:,1)))
axis image

figure('name','Green')
imagesc(squeeze(I(:,:,2)))
axis image
colorbar

figure('name','Blue')
imagesc(squeeze(I(:,:,3)))
axis image


I2 = rgb2hsv(I);
figure('name','Somme HSV')
imagesc(sum(I2,3))
axis image
colorbar


figure('name','Prod HSV')
imagesc(prod(I2,3))
axis image

figure('name','H')
imagesc(squeeze(I2(:,:,1)))
axis image

figure('name','S')
imagesc(squeeze(I2(:,:,2)))
axis image

figure('name','V')
imagesc(squeeze(I2(:,:,3)))
axis image
colorbar


cform = makecform('srgb2lab');
lab_he = double(applycform(I,cform));

figure('name','Somme lab')
imagesc(sum(lab_he,3))
axis image
colorbar

figure('name','Prod lab')
imagesc(prod(lab_he,3))
axis image
colorbar

figure('name','L')
imagesc(squeeze(lab_he(:,:,1)))
axis image
colorbar


figure('name','A')
imagesc(squeeze(lab_he(:,:,2)))
axis image

figure('name','B')
imagesc(squeeze(lab_he(:,:,3)))
axis image


G = im2double(I);

Varigreen = (squeeze(G(:,:,2)) - squeeze(G(:,:,1)))./(squeeze(G(:,:,2)) + squeeze(G(:,:,1)) - squeeze(G(:,:,3)));
figure('name','Varigreen')
imagesc(Varigreen)
axis image

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Might have some interesting function in the matlab image processing toolbox. Anyhow, some preprocessing of the image
%% for clarity, contrast etc.. might be useful !
M = imread('I.jpg');
M = imsharpen(M,'radius',15,'amount',0.1); 
Kaverage = filter2(fspecial('average',10),M);
figure;imagesc(Kaverage);colormap gray;axis image

B = bwboundaries(rgb2gray(M)); % B is supposed to be the boundary in a black and white picture. Doesn't work here, but w<ith some work on the image, who knows !


%% To smooth a picture, one can also take the average of the neighbors. The difference 

nbD = 5; %% Number of neighbors 
ligne = size(M,1);
colonne = size(M,2);

T = double(rgb2gray(M));

S = zeros(size(M,1),size(M,2),nbD);
for k = 1:nbD
    
    Zdown = [T((k + 1):ligne,:) ; zeros(k,colonne)];
    Zup = [zeros(k,colonne) ; T(1:(ligne - k),:)];
    Zleft = [zeros(ligne,k) T(:,1:(colonne - k))];
    Zright = [T(:,(k + 1):colonne) zeros(ligne,k)];
    
    S(:,:,k) = Zdown + Zup + Zleft + Zright;
end

T = mean(S,3);
figure
imagesc(T) %% imagesc is to plot matrix
colormap gray %% Then you chosse a colormap


T = var(S,0,3);
figure
imagesc(T) %% imagesc is to plot matrix
colormap gray %% Then you chosse a colormap


%% Function to detect goats based on maximum likelihood

% function Detect(Name,Chemin)

ch_Init = pwd;

% if nargin == 1
%     Chemin = ch_Init;
% end

load Proba.mat %% Proba is a matrix with the probability of a pixel being in a given value given the pixel is from a goat or weed or wood etc.. 
cd(Chemin)
I = double(imread(Name));
R = squeeze(I(:,:,1));
G = squeeze(I(:,:,2));
B = squeeze(I(:,:,3));

lab_he = double(rgb2lab(imread(Name)));
L = round(squeeze(lab_he(:,:,1)));
A = round(squeeze(lab_he(:,:,3)));
b = round(squeeze(lab_he(:,:,3)));

lab_he = double(rgb2lin(imread(Name)));
Lin1 = squeeze(lab_he(:,:,1));
Lin2 = squeeze(lab_he(:,:,3));
Lin3 = squeeze(lab_he(:,:,3));


Varigreen = (squeeze(I(:,:,2)) - squeeze(I(:,:,1)))./...
    (squeeze(I(:,:,2)) + squeeze(I(:,:,1)) - squeeze(I(:,:,3)));

M = G;
Diff_G = (M - mean(mean(M)).*ones)./(var(var(M)));

M = Varigreen;
Diff_V = (M - mean(mean(M)).*ones)./(var(var(M)));

X = [R(:) , G(:) , B(:) , L(:) , A(:) , b(:) , Lin1(:) , Lin2(:) , Lin3(:) , Varigreen(:) ,...
    Diff_G(:) , Diff_V(:)];
X(:,1) = X(:,1) + ones;
X(:,2) = X(:,2) + ones;
X(:,3) = X(:,3) + ones;

X(:,4) = X(:,4) + ones;
X(:,5) = X(:,5) + 301.*ones;
X(:,6) = X(:,6) + 301.*ones;

X(:,7) = X(:,7) + ones;
X(:,8) = X(:,8) + ones;
X(:,9) = X(:,9) + ones;
X(:,10) = round(double(round(X(:,10),2).*100 + 101.*ones));

R1 = [X(:,11) , -ones(size(X,1),1)];
R2 = [ones(1,numel(vG)) ; vG];
R = R1*R2;
R = R <=0;
[~ , Idx] = max(R,[],2);
X(:,11) = Idx;

R1 = [X(:,12) , -ones(size(X,1),1)];
R2 = [ones(1,numel(vV)) ; vV];
R = R1*R2;
R = R <=0;
[~ , Idx] = max(R,[],2);
X(:,12) = Idx;


% P = Proba_R(X(:,1),:).*Proba_G(X(:,2),:).*Proba_B(X(:,3),:).*Proba_L(X(:,4),:).*Proba_a(X(:,5),:)...
%     .*Proba_b(X(:,6),:).*Proba_L1(X(:,7),:).*Proba_L2(X(:,8),:).*Proba_L3(X(:,9),:).*Proba_V(X(:,10));
% 
% P = Proba_R(X(:,1),:) + Proba_G(X(:,2),:) + Proba_B(X(:,3),:) + Proba_L(X(:,4),:) + Proba_a(X(:,5),:)...
%     + Proba_b(X(:,6),:) + Proba_L1(X(:,7),:) + Proba_L2(X(:,8),:) + Proba_L3(X(:,9),:);

% P = Proba_R(X(:,1),:).*Proba_G(X(:,2),:).*Proba_B(X(:,3),:).*Proba_V(X(:,10),:);

P = Proba_G(X(:,2),:).*Proba_V(X(:,10),:).*exp(Proba_DiffG(X(:,11))).*exp(Proba_DiffV(X(:,12)));

% P = Proba_L1(X(:,7),:).*Proba_L2(X(:,8),:).*Proba_L3(X(:,9),:);

[~ , Res] = max(P,[],2);
figure
Res(1:6) = 1:6;
imagesc(reshape(Res,size(I,1),size(I,2)))
colormap([0 0 0 ; 1 1 1 ; 0.5 .5 .5 ; 0 .3 0 ; 0 0.6 0 ; 0 1 0])
labels = {'Goats','Wood 1','Wood 2','DWeed','AWeed','GWeed'};
lcolorbar(labels,'fontweight','bold');
axis image
figure
imshow(imread(Name))
cd(ch_Init);

%% Supervised learning using classification tree

% Fitting the classification tree based on I.jpg
I = double(imread('I.jpg'));
cform = makecform('srgb2lab');
lab_he = double(applycform(I,cform));


R = reshape(double(squeeze(lab_he(:,:,1))),[size(lab_he,1)*size(lab_he,2) , 1]);
G = reshape(double(squeeze(lab_he(:,:,2))),[size(lab_he,1)*size(lab_he,2) , 1]);
B = reshape(double(squeeze(lab_he(:,:,3))),[size(lab_he,1)*size(lab_he,2) , 1]);

Varigreen = reshape((squeeze(I(:,:,2)) - squeeze(I(:,:,1)))./(squeeze(I(:,:,2)) + squeeze(I(:,:,1)) - squeeze(I(:,:,3))),[size(lab_he,1)*size(lab_he,2) , 1]);
% Level of red, Green Blue and the Varigreen are our predictor variables.
% It means that the RGB value and the varigreen of a pixel will be used as
% predictor variable for classification

J = double(imread('I_OnlyGoats.JPG')); %% J is as I, but all pixles but goat are black
Idx = find(sum(J,3) ~=0); % Find the goats, i.e. pixels that are white

X = [R , G , B , Varigreen];
Y = zeros(size(X,1),1);
Y(Idx) = ones;% Y is the response variable, equals to 1 if pixel is a goat, 0 otherwise

Mdl = fitctree(X,Y,'PredictorNames',{'R','G','B','Varigreen'}) %% The laerning stage

% Model validation
I = double(imread('I2.jpg')); % Here it's a new picture and we want to classify all the pixels. 

%Let's compute the predictor for this new picture
cform = makecform('srgb2lab');
lab_he = double(applycform(I,cform));


R = reshape(double(squeeze(lab_he(:,:,1))),[size(lab_he,1)*size(lab_he,2) , 1]);
G = reshape(double(squeeze(lab_he(:,:,2))),[size(lab_he,1)*size(lab_he,2) , 1]);
B = reshape(double(squeeze(lab_he(:,:,3))),[size(lab_he,1)*size(lab_he,2) , 1]);

Varigreen = reshape((squeeze(I(:,:,2)) - squeeze(I(:,:,1)))./(squeeze(I(:,:,2)) + squeeze(I(:,:,1)) - squeeze(I(:,:,3))),[size(lab_he,1)*size(lab_he,2) , 1]);
X = [R , G , B , Varigreen];

C = Mdl.predict(X);

Res = reshape(C,[2400 2208]);
imagesc(Res) % It doesn't work here but with more learning, might be worst to try




