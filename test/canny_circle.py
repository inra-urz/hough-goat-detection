import cv2
import numpy as np

#img = cv2.imread('WithGoatsG2.png',0)
img = cv2.imread('TestPicS1.jpg',0)
#img = cv2.imread('1.png',0)

img = cv2.medianBlur(img,5)
img = cv2.Canny(img,100,200)
cimg = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)

circles = cv2.HoughCircles(img,cv2.HOUGH_GRADIENT,1,10,
                            param1=4,param2=9,minRadius=0,maxRadius=20)

circles = np.uint16(np.around(circles))
for i in circles[0,:]:
    # draw the outer circle
    cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
    # draw the center of the circle
    cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),3)

cv2.imshow('detected circles',cimg)
cv2.waitKey(0)
cv2.destroyAllWindows()
