import sys
import numpy as np
from scipy import ndimage
from PIL import Image

img = ndimage.imread("set/13.tif")
ker = ndimage.imread("set/Red/3_C.png")
out = ndimage.convolve(img, ker)
out = Image.fromarray(out)
out.save("test.png")